# Say 👋 hello 👋 to **Order66**

_Clone your files with the precision of the Kamino Cloning Facility_

![Kamino Cloning Facility](.assets/Kamino-Cloning-Facility.png)
> image source > [**Kamino Cloning Facility** in **Star Wars Battlefront II**](
https://battlefront.fandom.com/wiki/Kamino:_Cloning_Facility)

<br/>

<a target="_blank" href="https://www.paypal.com/donate/?business=8KS9JEFXKKWWA&no_recurring=1&item_name=to+support+AptApps&currency_code=GBP">
Support AptApps<br/>
<img width=100 src="https://www.paypalobjects.com/paypal-ui/logos/svg/paypal-color.svg"/>
</a><br/><br/>

<a target="_blank" href="https://gitlab.com/olfek/order66/-/packages">
<img src=".assets/download.svg" />
</a>

## Overview 🦅

Order66 has two ✌️ functions.  

  1. Facilitate the synchronization of files from one location to another. `Sync`
  2. Make the modification times of the contents of two locations equal. `TimeFixer`

We currently have 40+ unit tests. I believe I've covered most of the critical  
logic, but I will certainly have missed some stuff. Help to increase code  
coverage is more than welcome 🙂

File synchronization is done sequentially for maximum safety with only the  
initial hashing stage running with parallelism. File equality is determined  
using the [MD5 message-digest algorithm](https://en.wikipedia.org/wiki/MD5).  
There is currently no option to change this, if you want it, submit a MR/PR.

This program has been thoroughly tested repeatedly with various configurations  
on an important personal dataset of 40,000+ files over 20 GB in size.

## License 🪪

This project is licensed under the **"GNU Affero General Public License"**

[Details can be found HERE](
https://gitlab.com/olfek/order66/-/blob/main/LICENSE.txt)

## Contributions ✏️

I will aim to respond to issues within 48 hours.

I will aim to review bug fixes within 48 hours. New feature pull/merge requests  
may take longer.

New feature pull/merge requests can be submitted without any bureaucracy 💼,  
unit tests are the only requirement, and I reserve the right to reject MRs/PRs  
after a decent and proper explanation 🤗

## Where This Project Lives 🏠

  * **GitLab 🦊 (MAIN) (https://gitlab.com/olfek/order66)**
  * GitHub 🐙 (https://github.com/olfek/Order66) - Redirects to GitLab. Why?  
    GitHub is missing a lot of useful features that GitLab has.
  * AlternativeTo.net (https://alternativeto.net/software/order66/about) Show  
    some ❤️!

## Usage 🤔

When you execute the Order66 executable, you must choose one of the verbs  
(`Sync`/`TimeFixer`) below.

### Sync `Sync`

#### Options

| Option                                 | Type    | Expected Value             | Default | Required | Purpose                                                                                  |
|----------------------------------------|---------|----------------------------|---------|----------|------------------------------------------------------------------------------------------|
| Source                                 | string  | path to source directory   | null    | Yes      | The directory to sync FROM                                                               |
| Output                                 | string  | path to output directory   | null    | Yes      | The directory to sync TO                                                                 |
| Template                               | string  | path to template directory | null    | No       | The directory to use as a template, child paths will be converted to source paths.       |
| Rules                                  | string  | path to rules JSON file    | null    | No       | The rules to adhere to during sync                                                       |
| DryRun                                 | boolean | -                          | True    | No       | Perform a dry run to see what changes will be made **without** making any actual changes |
| RemoveStuffFromOutputThatIsExcluded    | boolean | -                          | False   | No       | Self explanatory                                                                         |
| RemoveStuffFromOutputThatIsNotInSource | boolean | -                          | False   | No       | Self explanatory                                                                         |
| RemoveEmptyDirsInOutput                | boolean | -                          | False   | No       | Self explanatory                                                                         |
| IgnoreEmptyDirsInSource                | boolean | -                          | False   | No       | Self explanatory                                                                         |

#### Invocation Example

```shell
./Order66 Sync --Source "/path/to/source/directory" --Output "/path/to/output/directory"
```

#### TXT Report

After the program has finished executing, there will be a report available on  
your desktop with a filename like this `Order66-Report-0001-01-31__00-00-00.txt`

### Time Fixer `TimeFixer`

#### Options

| Option   | Type   | Expected Value             | Default | Required | Purpose     |
|----------|--------|----------------------------|---------|----------|-------------|
| Source   | string | path to source directory   | null    | True     | _see above_ |
| Target   | string | path to target directory   | null    | True     | _see above_ |
| Template | string | path to template directory | null    | False    | _see above_ |
| Rules    | string | path to rules JSON file    | null    | False    | _see above_ |

#### Example

```shell
./Order66 TimeFixer --Source "/path/to/source/directory" --Target "/path/to/target/directory"
```

## Example `rules.json`

```json
{
  "Separator": "/",
  "Verbatim": [
    "/venv",
    "/node_modules",
    "/$RECYCLE.BIN",
    "/System Volume Information"
  ],
  "Regex": [
    "/\\.Trash-.*",
    "^/code(?:/|$)",
    "^/code-archived(?:/|$)",
    "^/VMs(?:/|$)"
  ],
  "DoNotDelete": [
    "/\\$RECYCLE\\.BIN",
    "/System Volume Information",
    "/\\.Trash-.*"
  ]
}
```

  * These rules will be tested against a "root relative path". For example. If  
    your `Source` argument is `/my/root/path`, then for the folder  
    `/my/root/path/myfolder`, the rules will be tested against the root relative  
    path `/myfolder`.
  * `Separator` is the path separator you have chosen to use in the rules above.
  * Values added in `Verbatim` are treated as **plain strings** and will be used  
    to perform a **string contains** check on the path in question. On  
    successful string contains check, the path in question will be excluded.
  * Values added in `Regex` are treated as **regular expressions** and will be  
    used to perform a **regex match** check on the path in question. On  
    successful regex match check, the path in question will be excluded.
  * Values added in `DoNotDelete` are treated as **regular expressions** and  
    will be used to perform a **regex match** check on the path in question. On  
    successful regex match check, the path in question will be protected from  
    deletion. This can be useful when the `Remove*` options above are _too_  
    inclusive.
  * **IMPORTANT❗**  
    Notice the `(?:/|$)` at the end. See what happens when it is removed.  
    https://regex101.com/r/RnAE8T/1