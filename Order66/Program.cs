using CommandLine;
using System;
using Order66.CLIOptions;
using Order66.IO;
using Order66.Sync_;
using Order66.TimeFixer;
using Order66.User;
using Order66.Utilities;

#if (DEBUG)
Console.ReadLine(); // To allow attaching debugger.
#endif

DuplexOut.StdOut = Console.Out;

Parser.Default.ParseArguments<SyncOptions, TimeFixerOptions>(args)
    .Register(new SyncRunnable())
    .Register(new TimeFixerRunnable(new RealIO(new Out())));