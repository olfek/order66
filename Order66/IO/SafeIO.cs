using System.IO;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Order66.Models;
using Order66.User;
using Order66.Utilities;

namespace Order66.IO;

public class SafeIO : BaseIO
{
    protected SafeIO(Out @out) : base(@out)
    {
    }

    protected override Collection<CleanPath> _ListDir(CleanPath directoryPath)
    {
        var items = Directory.EnumerateFileSystemEntries(
            directoryPath.Value,
            "*",
            new EnumerationOptions
            {
                RecurseSubdirectories = false,
                IgnoreInaccessible = false,
                ReturnSpecialDirectories = false,
                AttributesToSkip = 0
            }
        ).Select(directoryPath.CreateChild).ToList();

        return new Collection<CleanPath>(items);
    }

    protected override Collection<CleanPath> _ListAllFileSystemItems(CleanPath directoryPath)
    {
        var files = Directory.EnumerateFiles(
            directoryPath.Value,
            "*",
            new EnumerationOptions
            {
                RecurseSubdirectories = true,
                IgnoreInaccessible = true, // Best effort. Users problem if absent hash is needed.
                ReturnSpecialDirectories = false,
                AttributesToSkip = 0, // Don't even skip system files, because some we CAN read, e.g. lock files.
            }
        ).Select(directoryPath.CreateChild).ToList();

        return new Collection<CleanPath>(files);
    }

    protected override Times _ReadFileTimes(CleanPath path)
    {
        var strPath = path.Value;
        var write = File.GetLastWriteTime(strPath);
        var access = File.GetLastAccessTime(strPath);
        var create = File.GetCreationTime(strPath);
        return new Times(write, access, create);
    }

    protected override Times _ReadDirTimes(CleanPath path)
    {
        var strPath = path.Value;
        var write = Directory.GetLastWriteTime(strPath);
        var access = Directory.GetLastAccessTime(strPath);
        var create = Directory.GetCreationTime(strPath);
        return new Times(write, access, create);
    }

    protected override bool _DirExists(CleanPath path)
    {
        return Directory.Exists(path.Value);
    }

    protected override bool _FileExists(CleanPath path)
    {
        return File.Exists(path.Value);
    }

    protected override async Task<string> _GetHash(CleanPath a, CancellationToken ct)
    {
        await using var stream = File.Open(a.Value, FileMode.Open, FileAccess.Read);
        return await Helpers.GetMD5(stream, ct);
    }
}
