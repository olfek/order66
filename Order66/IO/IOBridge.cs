﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Order66.Models;

namespace Order66.IO;

public abstract class IOBridge
{
    protected virtual void _CreateDir(CleanPath path)
    {
        throw new NotImplementedException();
    }

    protected virtual void _DeleteDir(CleanPath path)
    {
        throw new NotImplementedException();
    }

    protected virtual void _DeleteFile(CleanPath path)
    {
        throw new NotImplementedException();
    }

    protected virtual void _CopyFile(CleanPath a, CleanPath b)
    {
        throw new NotImplementedException();
    }

    protected virtual void _MoveFile(CleanPath a, CleanPath b)
    {
        throw new NotImplementedException();
    }

    protected abstract bool _DirExists(CleanPath path);

    protected abstract bool _FileExists(CleanPath path);

    protected abstract Task<string> _GetHash(CleanPath a, CancellationToken ct);

    protected abstract Collection<CleanPath> _ListDir(CleanPath path);

    protected abstract Collection<CleanPath> _ListAllFileSystemItems(CleanPath dir);

    protected abstract Times _ReadFileTimes(CleanPath path);

    protected abstract Times _ReadDirTimes(CleanPath path);

    protected virtual void _WriteFileTimes(CleanPath path, Times times)
    {
        throw new NotImplementedException();
    }

    protected virtual void _WriteDirTimes(CleanPath path, Times times)
    {
        throw new NotImplementedException();
    }
}