using Order66.General;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Order66.Interfaces;
using Order66.Models;
using Order66.User;
using Order66.Utilities;

namespace Order66.IO;

// IMPORTANT NOTE:
// We do NOT need to perform a path exists check before every call to an underlying _ bridge implementation.
// This is because only some underlying _ bridge implementations NEED the check. For others it will be an overhead.
// Instead, we will do the check over in the specific underlying _ bridge implementation. For example. Dry IO.

public abstract class BaseIO : IOBridge, IOInterface
{
    private readonly Out _out;

    private FreqCounter<IOEvent> _methodCallCounter = new();

    protected BaseIO(Out @out)
    {
        _out = @out;
    }

    public FreqCounter<IOEvent> GetReadOnlyMethodCallCounter()
    {
        var counter = _methodCallCounter;
        _methodCallCounter = new FreqCounter<IOEvent>();
        counter.Close();
        return counter;
    }

    public Times ReadFileTimes(CleanPath path)
    {
        // Real IO doesn't handle non-existent path accordingly.
        // "GetCreationTime" returns "01/01/1601 00:00:00" for non existent things. Hence this exists check.
        if (!_FileExists(path))
            throw new PathDoesNotExistException(path);
        return _ReadFileTimes(path);
    }

    public Times ReadDirTimes(CleanPath path)
    {
        // Real IO doesn't handle non-existent path accordingly.
        // "GetCreationTime" returns "01/01/1601 00:00:00" for non existent things. Hence this exists check.
        if (!_DirExists(path))
            throw new PathDoesNotExistException(path);
        return _ReadDirTimes(path);
    }

    public void WriteFileTimes(CleanPath path, Times times) => _WriteFileTimes(path.CanWrite(), times);

    public void WriteDirTimes(CleanPath path, Times times) => _WriteDirTimes(path.CanWrite(), times);

    public Out GetOut() => _out;

    public void CopyFile(CleanPath a, CleanPath b)
    {
        b.CanWrite();
        _methodCallCounter.Count(IOEvents.Copy);
        _out.PrInteract.Path(IOEvents.Copy, a, b);
        _CopyFile(a, b);
    }

    public void MoveFile(CleanPath a, CleanPath b)
    {
        a.CanWrite();
        b.CanWrite();
        _methodCallCounter.Count(IOEvents.Move);
        _out.PrInteract.Path(IOEvents.Move, a, b);
        _MoveFile(a, b);
    }

    public void CreateDir(CleanPath path)
    {
        path.CanWrite();
        _methodCallCounter.Count(IOEvents.CreateDir);
        _out.PrInteract.Path(IOEvents.CreateDir, path);
        _CreateDir(path);
    }

    public void DeleteFile(CleanPath path)
    {
        path.CanWrite();
        _methodCallCounter.Count(IOEvents.DeleteFile);
        _out.PrInteract.Path(IOEvents.DeleteFile, path);
        _DeleteFile(path);
    }

    public void DeleteDir(CleanPath path)
    {
        path.CanWrite();
        _methodCallCounter.Count(IOEvents.DeleteDir);
        _out.PrInteract.Path(IOEvents.DeleteDir, path);
        _DeleteDir(path);
    }

    public bool DirExists(CleanPath path)
    {
        _methodCallCounter.Count(IOEvents.DirExists);
        return _DirExists(path);
    }

    public bool FileExists(CleanPath path)
    {
        _methodCallCounter.Count(IOEvents.FileExists);
        return _FileExists(path);
    }

    public IEnumerable<CleanPath> ListDir(CleanPath path)
    {
        _methodCallCounter.Count(IOEvents.List);
        return _ListDir(path);
    }

    public IEnumerable<CleanPath> ListAllFileSystemItems(CleanPath dir)
    {
        _methodCallCounter.Count(IOEvents.ListAll);
        return _ListAllFileSystemItems(dir);
    }

    public Task<string> GetHash(CleanPath a, CancellationToken ct)
    {
        // TODO: Count will be (and is) inaccurate due to parallel access.
        // OK for now, because this count is NOT shown to the user.
        // Update: I've decided to comment out the count.
        // Update: Need count for the unit tests. `IOEvents.Hash` commented out for now.
        // Order66.Tests\MainTests.cs:211
        // Order66.Tests\MainTests.cs:313
        // _methodCallCounter.Count(IOEvents.Hash);
        return _GetHash(a, ct);
    }

    public void RestoreDirTimes(CleanPath a, CleanPath b)
    {
        b.CanWrite();
        _methodCallCounter.Count(IOEvents.RestoreDirTimes);
        _out.PrInteract.Path(IOEvents.RestoreDirTimes, a, b);
        WriteDirTimes(b, ReadDirTimes(a));
    }
}
