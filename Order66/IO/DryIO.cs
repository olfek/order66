using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Order66.Interfaces;
using Order66.Models;
using Order66.User;
using Order66.Utilities;

namespace Order66.IO;

public class DryIO : SafeIO
{
    private readonly IExposedIO? _io;

    public readonly IOCassette Cassette = new();

    public DryIO(Out? @out, IExposedIO? io = null) : base(@out ?? new Out())
    {
        _io = io;
    }

    protected override void _CreateDir(CleanPath path)
    {
        while (path.RootRelativePath != null)
        {
            Cassette.MarkDirExists(path, out var tuple);
            path = tuple.ParentPath;
        }
    }

    protected override void _WriteDirTimes(CleanPath path, Times times)
    {
        if (!_DirExists(path))
            throw new PathDoesNotExistException(path);
    }

    protected override void _WriteFileTimes(CleanPath path, Times times)
    {
        if (!_FileExists(path))
            throw new PathDoesNotExistException(path);
    }

    protected override void _DeleteFile(CleanPath path)
    {
        Cassette.MarkFileDoesNotExist(path);
    }

    protected override void _DeleteDir(CleanPath path)
    {
        Cassette.MarkDirDoesNotExist(path);
    }

    protected override void _CopyFile(CleanPath a, CleanPath b)
    {
        FromAndToExistsCheck(a, b);
        Cassette.MarkFileExists(b);
    }

    protected override void _MoveFile(CleanPath a, CleanPath b)
    {
        FromAndToExistsCheck(a, b);
        Cassette.MarkFileDoesNotExist(a);
        Cassette.MarkFileExists(b);
    }

    protected override bool _FileExists(CleanPath path)
    {
        if (!Cassette.IsHierarchyIntact(path))
            return false;
        if (Cassette.FileExists(path, out var exists)) // We prioritize this.
            return exists;
        return _io?.ExFileExists(path) ?? base._FileExists(path);
    }

    protected override bool _DirExists(CleanPath path)
    {
        if (path.Root.Equals(path))
            return true; // If root path, assume exists, because we cannot capture paths with no parent.
        if (!Cassette.IsHierarchyIntact(path))
            return false;
        if (Cassette.DirExists(path, out var exists)) // We prioritize this.
            return exists;
        return _io?.ExDirExists(path) ?? base._DirExists(path);
    }

    protected override Task<string> _GetHash(CleanPath a, CancellationToken ct)
    {
        return _io?.ExGetHash(a, ct) ?? base._GetHash(a, ct);
    }

    protected override Collection<CleanPath> _ListDir(CleanPath directoryPath)
    {
        if (!_DirExists(directoryPath))
            throw new PathDoesNotExistException(directoryPath);

        var items = new Collection<CleanPath>();

        try
        {
            items = _io?.ExListDir(directoryPath) ?? base._ListDir(directoryPath);
        }
        catch (DirectoryNotFoundException)
        {
        }

        Cassette.ReplayAllStatesDirect(
            directoryPath,
            path =>
            {
                if (items.Contains(path)) return;
                items.Add(path);
            },
            path => items.Remove(path));

        return items;
    }

    protected override Collection<CleanPath> _ListAllFileSystemItems(CleanPath directoryPath)
    {
        if (!_DirExists(directoryPath))
            throw new PathDoesNotExistException(directoryPath);

        var items = new Collection<CleanPath>();

        try
        {
            items = _io?.ExListAllFileSystemItems(directoryPath) ?? base._ListAllFileSystemItems(directoryPath);
        }
        catch (DirectoryNotFoundException)
        {
        }

        Cassette.ReplayFileStateIndirect(
            directoryPath,
            path =>
            {
                if (items.Contains(path)) return;
                items.Add(path);
            },
            path => items.Remove(path));

        return new Collection<CleanPath>(items.Where(Cassette.IsHierarchyIntact).ToArray());
    }

    private void FromAndToExistsCheck(CleanPath a, CleanPath b)
    {
        if (!_FileExists(a))
            throw new PathDoesNotExistException(a);

        var bParentPath = b.GetParentPathAndNameTuple().ParentPath;
        if (!_DirExists(bParentPath))
            throw new PathDoesNotExistException(bParentPath);
    }
}
