using System.IO;
using Order66.Models;
using Order66.User;

namespace Order66.IO;

public class RealIO : SafeIO
{
    public RealIO(Out @out) : base(@out)
    {
    }

    protected override void _CreateDir(CleanPath path)
    {
        Directory.CreateDirectory(path.CanWrite().Value);
    }

    protected override void _DeleteFile(CleanPath path)
    {
        File.Delete(path.CanWrite().Value);
    }

    protected override void _DeleteDir(CleanPath path)
    {
        Directory.Delete(path.CanWrite().Value, true);
    }

    protected override void _CopyFile(CleanPath a, CleanPath b)
    {
        File.Copy(a.Value, b.CanWrite().Value);
    }

    protected override void _MoveFile(CleanPath a, CleanPath b)
    {
        File.Move(a.CanWrite().Value, b.CanWrite().Value);
    }

    protected override void _WriteFileTimes(CleanPath path, Times times)
    {
        var strPath = path.CanWrite().Value;
        File.SetLastWriteTime(strPath, times.Write);
        File.SetLastAccessTime(strPath, times.Access);
        File.SetCreationTime(strPath, times.Create);
    }

    protected override void _WriteDirTimes(CleanPath path, Times times)
    {
        var strPath = path.CanWrite().Value;
        Directory.SetLastWriteTime(strPath, times.Write);
        Directory.SetLastAccessTime(strPath, times.Access);
        Directory.SetCreationTime(strPath, times.Create);
    }
}
