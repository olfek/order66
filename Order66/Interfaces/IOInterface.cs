using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Order66.Models;
using Order66.User;
using Order66.Utilities;

namespace Order66.Interfaces;

public interface IOInterface
{
    // Things which DO NOT CHANGE the file system below.

    Task<string> GetHash(CleanPath a, CancellationToken ct);

    IEnumerable<CleanPath> ListDir(CleanPath directoryPath);

    IEnumerable<CleanPath> ListAllFileSystemItems(CleanPath dir);

    bool FileExists(CleanPath path);

    bool DirExists(CleanPath path);

    // Things which DO CHANGE the file system below.

    /// <summary>
    /// Make times of b same as a.
    /// </summary>
    void RestoreDirTimes(CleanPath a, CleanPath b);

    void DeleteFile(CleanPath path);

    void CopyFile(CleanPath a, CleanPath b);

    void MoveFile(CleanPath a, CleanPath b);

    void DeleteDir(CleanPath path);

    void CreateDir(CleanPath path);

    FreqCounter<IOEvent> GetReadOnlyMethodCallCounter();

    Times ReadFileTimes(CleanPath path);

    Times ReadDirTimes(CleanPath path);

    void WriteFileTimes(CleanPath path, Times times);

    void WriteDirTimes(CleanPath path, Times times);

    // Other

    Out GetOut();
}
