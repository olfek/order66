﻿namespace Order66.Interfaces;

public interface IRunnable<TInput>
{
    public void Run(TInput o);
}