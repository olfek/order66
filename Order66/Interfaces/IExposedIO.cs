﻿using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Order66.Models;

namespace Order66.Interfaces;

public interface IExposedIO
{
    public Collection<CleanPath> ExListDir(CleanPath directoryPath);

    public Collection<CleanPath> ExListAllFileSystemItems(CleanPath directoryPath);

    public bool ExFileExists(CleanPath path);

    public bool ExDirExists(CleanPath path);

    public Task<string> ExGetHash(CleanPath a, CancellationToken ct);
}