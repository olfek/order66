using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Order66.Models;

namespace Order66.Sync_;

public partial class Sync
{
    private readonly SemaphoreSlim ConsoleLock = new(1, 1);

    private IEnumerable<Func<Task>> StoreFileHashes(
        CleanPath rootPath,
        IDictionary<string, List<SingleUsePath>> hashToPathsMap,
        IDictionary<CleanPath, string> pathToHashMap,
        CancellationTokenSource cts)
    {
        var ct = cts.Token;
        foreach (var cleanPath in _io.ListAllFileSystemItems(rootPath))
        {
            if (_rules.IsExcluded(cleanPath))
            {
                continue;
            }

            async Task Bbb()
            {
                var fileHash = await _io.GetHash(cleanPath, ct);

                await ConsoleLock.WaitAsync(ct);
                {
                    pathToHashMap[cleanPath] = fileHash;

                    var singleUsePath = new SingleUsePath(cleanPath);
                    if (hashToPathsMap.TryGetValue(fileHash, out var singleUsePaths))
                    {
                        singleUsePaths.Add(singleUsePath);
                    }
                    else
                    {
                        hashToPathsMap[fileHash] = new List<SingleUsePath> { singleUsePath };
                    }

                    _progress.ReportProgress();
                }
                ConsoleLock.Release();
            }

            yield return () =>
            {
                async Task Aaa()
                {
                    try
                    {
                        await Task.Run(Bbb, ct);
                    }
                    catch (Exception e)
                    {
                        cts.Cancel();
                        if (e is not TaskCanceledException && e is not OperationCanceledException) throw;
                    }
                }

                return Aaa();
            };
        }
    }

    private void BlockingStoreFileHashesInParallel(int maxIOThreads)
    {
        using var cts = new CancellationTokenSource();

        var tasksToExecute =
            StoreFileHashes(SourceRoot, _sourceHashToPathsMap, _sourcePathToHashMap, cts).Concat(
                StoreFileHashes(OutputRoot, _outputHashToPathsMap, _outputPathToHashMap, cts)).ToArray();

        var skip = 0;

        while (true)
        {
            var batch = tasksToExecute.Skip(skip).Take(maxIOThreads).ToArray();
            skip += maxIOThreads;

            if (batch.Length <= 0) break;

            try
            {
                Task.WaitAll(batch.Select(getTask => getTask()).ToArray());
            }
            catch (AggregateException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}