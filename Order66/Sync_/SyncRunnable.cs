using System;
using System.Collections.Generic;
using System.IO;
using ConsoleTables;
using Order66.CLIOptions;
using Order66.Interfaces;
using Order66.IO;
using Order66.Models;
using Order66.User;
using Order66.Utilities;

namespace Order66.Sync_;

internal class SyncRunnable : IRunnable<SyncOptions>
{
    public void Run(SyncOptions o)
    {
        var now = DateTime.UtcNow;

        var @out = new Out(new MemoryStream());

        @out.WriteLineFile("Order66 Report");
        @out.WriteLineFile(now.ToLongDateString() + " - " + now.ToLongTimeString() + " (UTC)");
        @out.WriteLineFileAndStdOut();

        TableGenerator.FromObjectProps(@out, o);
        @out.PrInteract.UserConfirmationPrompt();

        var success = false;
        var perStageEventCount = new List<LabeledData<FreqCounterPair>>();

        if (o.Source == null || o.Output == null)
        {
            @out.WriteLineFileAndStdOut("Source and/or Output is null");
            goto DumpFile;
        }

        if (!Directory.Exists(o.Source) || !Directory.Exists(o.Output))
        {
            @out.WriteLineFileAndStdOut("Source and/or Output does not exist");
            goto DumpFile;
        }

        IOInterface io = new DryIO(@out);

        if (o.DryRun == BoolType.False)
        {
            io = new RealIO(@out);
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Green;
            @out.WriteLineFileAndStdOut("/!\\ DRY RUN /!\\");
            Console.ResetColor();
        }

        @out.PrInteract.StageSeparator();

        if (o.Template != null) o.Template = Path.GetFullPath(o.Template);
        var app = new Sync(
            io,
            Path.GetFullPath(o.Source),
            Path.GetFullPath(o.Output),
            @out,
            Rules.FromPathOrDefault(o.Rules))
        {
            Options = o
        };

        try
        {
            perStageEventCount = app.Execute();
            success = true;
        }
        catch (Exception e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            @out.WriteLineFileAndStdOut("ERROR: " + e.Message);
            @out.WriteLineFileAndStdOut(e.ToString());
            Console.ResetColor();
        }

        DumpFile:

        if (success)
        {
            @out.WriteLineFileAndStdOut($"*** Summary Report ***{Environment.NewLine}");
            TableGenerator.FromIOEvents(@out, perStageEventCount).Write(Format.Alternative);
        }

        var outputReportFilePath = Path.GetFullPath(Path.Join(
            Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
            "Order66-Report-" + now.ToString("yyyy-MM-dd__HH-mm-ss") + ".txt"));

        Console.WriteLine(
            "See full output at:" + Environment.NewLine + outputReportFilePath + Environment.NewLine);

        @out.Flush();
        @out.Stream?.Seek(0, SeekOrigin.Begin);
        using var fileStream = new FileStream(outputReportFilePath, FileMode.OpenOrCreate, FileAccess.Write);
        @out.Stream?.CopyTo(fileStream);
        @out.Close();
    }
}