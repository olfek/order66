using System.Diagnostics.CodeAnalysis;
using Order66.General;
using Order66.Models;

namespace Order66.Sync_;

[SuppressMessage("ReSharper", "InvertIf")]
partial class Sync
{
    /// <summary>
    /// Copy files from source to output that do not already exist in output.
    /// Optionally copy empty directories.
    /// </summary>
    /// <param name="parentSourcePath"></param>
    /// <param name="parentOutputPath"></param>
    /// <param name="templatePath"></param>
    /// <returns></returns>
    private RecursionReport WriteOutput(
        CleanPath parentSourcePath,
        CleanPath? parentOutputPath,
        CleanPath? templatePath)
    {
        var report = new RecursionReport();

        foreach (var dynPath in _io.ListDir(templatePath ?? parentSourcePath))
        {
            _progress.ReportProgress();

            var pathInSource = templatePath != null ? dynPath.WithNewRoot(parentSourcePath) : dynPath;
            var pathInOutput = pathInSource.WithNewRoot(OutputRoot);

            if (_rules.IsExcluded(pathInSource))
            {
                _out.PrInteract.Path(IOEvents.Excluded, pathInSource);
                WriteOutputEventCounter.Count(IOEvents.Excluded);
                continue;
            }

            if (_io.DirExists(pathInSource))
            {
                // Complete report. We CAN act on it.
                var childReport = templatePath != null
                    ? WriteOutput(parentSourcePath, pathInOutput, dynPath)
                    : WriteOutput(pathInSource, pathInOutput, null);
                report.Update(childReport);

                if (childReport.Modified)
                {
                    _io.RestoreDirTimes(pathInSource, pathInOutput);
                }

                if (!childReport.FilesExist)
                {
                    WriteOutputEventCounter.Count(IOEvents.EmptyDir);

                    if (!Options.IgnoreEmptyDirsInSource)
                    {
                        if (!_io.DirExists(pathInOutput))
                        {
                            _io.CreateDir(pathInOutput);
                            _io.RestoreDirTimes(pathInSource, pathInOutput);
                            report.Modified = true;
                        }
                        report.ParentDirExists = true;
                    }
                }
            }
            else if (_io.FileExists(pathInSource))
            {
                report.FilesExist = true; // This will be made TRUE literally by the end of the block, if not already literally TRUE.

                if (_io.FileExists(pathInOutput))
                {
                    report.ParentDirExists = true;

                    var hashA = _sourcePathToHashMap[pathInSource];
                    var hashB = _outputPathToHashMap[pathInOutput];

                    if (hashA == hashB)
                    {
                        continue;
                    }

                    WriteOutputEventCounter.Count(IOEvents.ReplaceFile);
                    _out.PrInteract.Path(IOEvents.ReplaceFile, pathInOutput);
                    report.Modified = true;
                    _io.DeleteFile(pathInOutput);
                }
                else if (parentOutputPath != null && !report.ParentDirExists)
                {
                    if (!_io.DirExists(parentOutputPath)) // Don't need this, but kept for test expectations.
                    {
                        _io.CreateDir(parentOutputPath);
                    }
                    report.ParentDirExists = true;
                }

                report.Modified = true;
                _io.CopyFile(pathInSource, pathInOutput);
                _outputPathToHashMap[pathInOutput] = _sourcePathToHashMap[pathInSource];
            }
        }

        return report;
    }
}