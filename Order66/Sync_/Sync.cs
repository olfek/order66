using System;
using System.Collections.Generic;
using System.Threading;
using Order66.CLIOptions;
using Order66.Enums;
using Order66.Interfaces;
using Order66.Models;
using Order66.User;
using Order66.Utilities;

namespace Order66.Sync_;

public partial class Sync
{
    private readonly IOInterface _io;

    private readonly Out _out;

    public readonly CleanPath OutputRoot;

    private readonly List<LabeledData<FreqCounterPair>> _perStageEventCount = new();

    private readonly Rules _rules;

    public readonly CleanPath SourceRoot;

    public readonly FreqCounter<IOEvent> CleanOutputEventCounter = new();

    public readonly FreqCounter<IOEvent> WriteOutputEventCounter = new();

    private Progress _progress;

    private int _stageCount;

    public SyncOptions Options = new();

    public Sync(IOInterface io, string sourceRoot, string outputRoot, Out? @out = null, Rules? rules = null)
    {
        _io = io;
        _out = @out ?? new Out();
        _progress = new Progress();
        _rules = rules ?? new Rules();
        // ReSharper disable once RedundantArgumentDefaultValue
        SourceRoot = new CleanPath(sourceRoot, _rules, Target.Source);
        OutputRoot = new CleanPath(outputRoot, _rules, Target.Output);

        _out.PrInteract.Header("hashing files in source and output", _stageCount);

        ThreadPool.GetMinThreads(out var minWorkerThreads, out var minIOThreads);
        ThreadPool.GetMaxThreads(out _, out var maxIOThreads);

        ThreadPool.SetMinThreads(minWorkerThreads, maxIOThreads);
        BlockingStoreFileHashesInParallel(maxIOThreads);
        ThreadPool.SetMinThreads(minWorkerThreads, minIOThreads); // No parallelism beyond this point.
    }

    public RecursionReport ExecutePrepareOutput() => CleanOutput(_io.ListDir(OutputRoot));

    public RecursionReport ExecuteWriteOutput() => WriteOutput(SourceRoot, null,
        Options.Template == null ? null : new CleanPath(Options.Template, _rules));

    public List<LabeledData<FreqCounterPair>> Execute()
    {
        PostRunStage(null, false); // For stage zero the hashing stage

        RunStage("relocating moved files", () => TrackFileMovesAndMoveFiles());
        RunStage("prepare output", () => ExecutePrepareOutput(), CleanOutputEventCounter);
        RunStage("write output", () => ExecuteWriteOutput(), WriteOutputEventCounter);

        return _perStageEventCount;
    }

    private void RunStage(string header, Action action, FreqCounter<IOEvent>? counter = null)
    {
        _stageCount++;
        _out.PrInteract.Header(header, _stageCount);
        action();
        PostRunStage(counter);
    }

    private void PostRunStage(FreqCounter<IOEvent>? counter, bool addToPerStageEventCount = true)
    {
        // Clears method call counter too.
        var freqCounterPair = new FreqCounterPair(counter, _io.GetReadOnlyMethodCallCounter());
        var labeledData = new LabeledData<FreqCounterPair>($"STAGE {_stageCount}", freqCounterPair);
        if (addToPerStageEventCount)
            _perStageEventCount.Add(labeledData);
        _progress.ClearConsole();
        _out.PrInteract.SpaceAfterPrintedPaths();
        _out.PrInteract.PrintProgress(_progress);
        _out.PrInteract.StageSeparator();
        _out.PrInteract = _out.PrInteract.Reset();
        _progress = new Progress();
    }
}