using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Order66.General;
using Order66.Models;

namespace Order66.Sync_;

[SuppressMessage("ReSharper", "InvertIf")]
partial class Sync
{
    /// <summary>
    /// Optionally remove files and directories from output that are excluded.
    /// Optionally remove files and directories from output that are not in source.
    /// Optionally remove empty directories in output.
    /// </summary>
    /// <param name="outputPaths"></param>
    /// <returns></returns>
    private RecursionReport CleanOutput(IEnumerable<CleanPath> outputPaths)
    {
        var report = new RecursionReport();

        if (Options is
            {
                RemoveStuffFromOutputThatIsExcluded: false,
                RemoveStuffFromOutputThatIsNotInSource: false,
                RemoveEmptyDirsInOutput: false
            })
        {
            return report; // Nothing to be done.
        }

        foreach (var pathInOutput in outputPaths)
        {
            _progress.ReportProgress();

            if (!_rules.CanRemove(pathInOutput))
            {
                report.FilesExist = true;
                continue;
            }

            var pathInSource = pathInOutput.WithNewRoot(SourceRoot);
            var pathIsExcluded = false;

            if (Options.RemoveStuffFromOutputThatIsExcluded && _rules.IsExcluded(pathInOutput))
            {
                pathIsExcluded = true;
                _out.PrInteract.Path(IOEvents.Excluded, pathInOutput);
                CleanOutputEventCounter.Count(IOEvents.Excluded);
            }

            if (_io.DirExists(pathInOutput))
            {
                var notInSource = Options.RemoveStuffFromOutputThatIsNotInSource && !_io.DirExists(pathInSource);
                if (pathIsExcluded || notInSource)
                {
                    if (notInSource)
                    {
                        _out.PrInteract.Path(IOEvents.DirNotInSource, pathInOutput);
                        CleanOutputEventCounter.Count(IOEvents.DirNotInSource);
                    }
                    _io.DeleteDir(pathInOutput);
                    report.Modified = true;
                }
                else
                {
                    // Complete report. We CAN act on it.
                    var childReport = CleanOutput(_io.ListDir(pathInOutput));
                    report.Update(childReport);

                    if (!childReport.FilesExist && Options.RemoveEmptyDirsInOutput)
                    {
                        _out.PrInteract.Path(IOEvents.EmptyDir, pathInOutput);
                        CleanOutputEventCounter.Count(IOEvents.EmptyDir);
                        _io.DeleteDir(pathInOutput);
                        report.Modified = true;
                    }
                    else if (childReport.Modified)
                    {
                        try
                        {
                            _io.RestoreDirTimes(pathInSource, pathInOutput);
                        }
                        catch (PathDoesNotExistException e) when (ReferenceEquals(e.Path, pathInSource))
                        {
                            // OK.
                            // For case where `Options.RemoveStuffFromOutputThatIsNotInSource` is FALSE
                            // and we descended into a directory which does NOT exist in SOURCE.
                        }
                    }
                }
            }
            else if (_io.FileExists(pathInOutput))
            {
                var notInSource = Options.RemoveStuffFromOutputThatIsNotInSource && !_io.FileExists(pathInSource);
                if (pathIsExcluded || notInSource)
                {
                    if (notInSource)
                    {
                        _out.PrInteract.Path(IOEvents.FileNotInSource, pathInOutput);
                        CleanOutputEventCounter.Count(IOEvents.FileNotInSource);
                    }
                    _io.DeleteFile(pathInOutput);
                    report.Modified = true;
                }
                else
                {
                    report.FilesExist = true;
                }
            }
        }

        return report;
    }
}