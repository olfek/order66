using System;
using System.Collections.Generic;
using System.Linq;
using Order66.Models;
using Order66.Utilities;

namespace Order66.Sync_;

public partial class Sync
{
    private readonly Dictionary<string, List<SingleUsePath>> _sourceHashToPathsMap = // Hash -> Path
        new(StringComparer.InvariantCultureIgnoreCase);

    private readonly Dictionary<string, List<SingleUsePath>> _outputHashToPathsMap = // Hash -> Path
        new(StringComparer.InvariantCultureIgnoreCase);

    private readonly Dictionary<CleanPath, string> _sourcePathToHashMap = new(); // Path -> Hash

    private readonly Dictionary<CleanPath, string> _outputPathToHashMap = new(); // Path -> Hash

    public (List<FileMoveInfo> MoveJobs, SortedSet<string> PreMovePaths, SortedSet<string> PostMovePaths)
        TrackFileMovesAndMoveFiles()
    {
        var moveJobs = new List<FileMoveInfo>();
        var postMovePaths = new SortedSet<string>(LongestStringFirstComparer.Static);
        var preMovePaths = new SortedSet<string>(LongestStringFirstComparer.Static);

        foreach (var (fileHash, fileOutputPaths) in _outputHashToPathsMap)
        {
            _progress.ReportProgress();

            if (!_sourceHashToPathsMap.TryGetValue(fileHash, out var fileSourcePaths)) continue;

            foreach (var fileSourcePath in fileSourcePaths) // mark source/output paths as used if used.
            {
                _progress.ReportProgress();

                // going through source paths for first time, so no if used check.

                foreach (var fileOutputPath in fileOutputPaths)
                {
                    _progress.ReportProgress();

                    if (fileOutputPath.Used) continue;

                    var sourceTuple = fileSourcePath.Path.GetRootRelativeParentPathAndNameTuple();
                    var outputTuple = fileOutputPath.Path.GetRootRelativeParentPathAndNameTuple();

                    if (
                        // We want to ignore case for RootRelativeParentPath because a file move
                        // will have no effect.
                        string.Equals(
                            sourceTuple.RootRelativeParentPath, outputTuple.RootRelativeParentPath,
                            StringComparison.InvariantCultureIgnoreCase) &&
                        // We want to be case sensitive for Name so renames work.
                        sourceTuple.Name.Equals(outputTuple.Name, StringComparison.InvariantCulture))
                    {
                        fileSourcePath.Used = fileOutputPath.Used = true;
                        break;
                    }
                }
            }

            foreach (var fileSourcePath in fileSourcePaths)
            {
                _progress.ReportProgress();

                if (fileSourcePath.Used) continue;

                foreach (var fileOutputPath in fileOutputPaths)
                {
                    _progress.ReportProgress();

                    if (fileOutputPath.Used) continue;

                    fileSourcePath.Used = fileOutputPath.Used = true;
                    // TODO: Attempt to best guess new path from available paths rather than using the first one.
                    var moveJob = new FileMoveInfo(fileOutputPath.Path, fileSourcePath.Path.WithNewRoot(OutputRoot));
                    moveJobs.Add(moveJob);

                    foreach (var preMovePath in moveJob.Before.AllRootRelativeParentPaths())
                    {
                        preMovePaths.Add(preMovePath);
                    }

                    foreach (var postMovePath in moveJob.After.AllRootRelativeParentPaths())
                    {
                        postMovePaths.Add(postMovePath);
                    }

                    break;
                }
            }
        }

        foreach (var job in moveJobs)
        {
            _progress.ReportProgress();

            _io.CreateDir(job.After.GetParentPathAndNameTuple().ParentPath);
            _io.MoveFile(job.Before, job.After);
            _outputPathToHashMap[job.After] = _outputPathToHashMap[job.Before];
        }

        var pathsWithFiles = new List<CleanPath>();

        foreach (var preMovePath in preMovePaths)
        {
            _progress.ReportProgress();

            var oldPathInOutput = OutputRoot.CreateChild(OutputRoot.CanWrite().Value + preMovePath);
            var oldPathInSource = SourceRoot.CreateChild(SourceRoot.Value + preMovePath);

            var oldPathInOutputMayBeEmpty =
                !pathsWithFiles.Any(path => path.ChildOf(oldPathInOutput) || path.Equals(oldPathInOutput));

            if (oldPathInOutputMayBeEmpty)
            {
                if (!_io.ListAllFileSystemItems(oldPathInOutput).Any() && !_io.DirExists(oldPathInSource))
                {
                    _io.DeleteDir(oldPathInOutput);
                    continue;
                }

                pathsWithFiles.Add(oldPathInOutput); // Reduce calls to `ListAllFileSystemItems`
            }

            try
            {
                _io.RestoreDirTimes(oldPathInSource, oldPathInOutput);
            }
            catch (PathDoesNotExistException e) when (ReferenceEquals(e.Path, oldPathInSource)) // Source not existing is fine
            {
                // Do nothing.
            }
        }

        foreach (var postMovePath in postMovePaths) // Any paths not existing here is a problem. Allow exception to propagate.
        {
            _progress.ReportProgress();

            _io.RestoreDirTimes(
                SourceRoot.CreateChild(SourceRoot.Value + postMovePath),
                OutputRoot.CreateChild(OutputRoot.CanWrite().Value + postMovePath));
        }

        return (moveJobs, preMovePaths, postMovePaths);
    }
}