
namespace Order66.General;

public static class ErrorMessages
{
    public const string RootPathTooLong = "Root path longer than child path.";
    public const string RootPathNotInChildPath = "Root path not part of child path.";
    public const string NullOrSourceTargetReadOnlyError = "Null target and source is read-only!";
}
