using System;
using Order66.Models;

namespace Order66.General;

public static class IOEvents
{
    public static readonly IOEvent ReplaceFile = new("REPLACE FILE");

    public static readonly IOEvent Excluded = new("EXCLUDED");

    public static readonly IOEvent EmptyDir = new("EMPTY DIR");

    public static readonly IOEvent FileNotInSource = new("NOT IN SOURCE (FILE)");

    public static readonly IOEvent DirNotInSource = new("NOT IN SOURCE (DIR)");

    public static readonly IOEvent List = new("LIST");

    public static readonly IOEvent ListAll = new("LIST ALL");

    public static readonly IOEvent CreateDir = new("CREATE DIR");

    public static readonly IOEvent RestoreDirTimes = new("RESTORE DIR TIMES");

    public static readonly IOEvent Hash = new("HASH");

    public static readonly IOEvent DeleteFile = new("DELETE FILE");

    public static readonly IOEvent DeleteDir = new("DELETE DIR");

    public static readonly IOEvent Copy = new("COPY");

    public static readonly IOEvent Move = new("MOVE");

    public static readonly IOEvent DirExists = new("DIR EXISTS");

    public static readonly IOEvent FileExists = new("FILE EXISTS");

    private static int _maxLength = -1;

    public static int GetMaxLength()
    {
        if (_maxLength != -1)
        {
            return _maxLength;
        }

        var fields = typeof(IOEvents).GetFields();

        var maxLength = -1;

        foreach (var field in fields)
        {
            if (field.GetValue(null)! is IOEvent fieldValue && fieldValue.Description.Length > maxLength)
            {
                maxLength = fieldValue.Description.Length;
            }
        }

        if (maxLength == -1)
        {
            throw new ArgumentException("Max length still -1", nameof(maxLength));
        }

        return _maxLength = maxLength;
    }
}
