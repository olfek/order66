﻿using System;

namespace Order66.Models;

public record Times(DateTime Write, DateTime Access, DateTime Create);