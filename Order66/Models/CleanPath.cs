using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Order66.Enums;
using Order66.General;
using Order66.Utilities;

namespace Order66.Models;

public class CleanPath : IComparable<CleanPath>
{
    private readonly CleanPath _root;

    private readonly Target _target;

    public readonly string Value;

    public readonly Rules Rules;

    public CleanPath(string path, Rules? rules = null, Target target = Target.Source)
    {
        // TODO: Do `Path.GetFullPath` here so we are guaranteed to be working with absolute paths.
        if (string.IsNullOrWhiteSpace(path))
        {
            throw new Exception("Path is null or empty");
        }

        rules ??= new Rules();

        var otherSeparator = rules.Separator switch
        {
            '/' => '\\',
            '\\' => '/',
            _ => throw new Exception("Chosen separator is invalid.")
        };

        var value = path.Replace(otherSeparator, rules.Separator).TrimEnd(rules.Separator);
        // Should we also strip leading path separators? - No because what about relative paths?

        if (value.StartsWith(rules.Separator))
        {
            value = rules.Separator + value.TrimStart(rules.Separator);
        }

        Rules = rules;
        Value = value;
        _target = target;
        Root = this;
    }

    private int Length => Value.Length;

    /// <summary>
    /// Root location, will be a "source" or "output" root path.
    /// </summary>
    public CleanPath Root
    {
        get => _root;
        [MemberNotNull(nameof(_root))]
        private init
        {
            CheckRootPathAndChildPathCompatibility(value, this);
            _root = value;
        }
    }

    public string GetName()
    {
        var lastSeparatorIndex = Value.LastIndexOf(Rules.Separator);

        if (lastSeparatorIndex < 0 || lastSeparatorIndex >= Value.Length - 1)
            throw new InvalidOperationException(
                $"Cannot derive name for '{this}'");

        return Value[(lastSeparatorIndex + 1)..];
    }

    public (CleanPath ParentPath, string Name) GetParentPathAndNameTuple()
    {
        var lastSeparatorIndex = Value.LastIndexOf(Rules.Separator);

        if (lastSeparatorIndex <= 0 || lastSeparatorIndex >= Value.Length - 1)
            throw new InvalidOperationException(
                $"Cannot derive parent path AND/OR name for '{this}'");

        var name = Value[(lastSeparatorIndex + 1)..];
        var parentPath = new CleanPath(Value[..lastSeparatorIndex], Rules, _target) { Root = Root };
        return (parentPath, name);
    }

    public (string? RootRelativeParentPath, string Name) GetRootRelativeParentPathAndNameTuple()
    {
        var tuple = GetParentPathAndNameTuple();
        return (tuple.ParentPath.RootRelativePath, tuple.Name);
    }

    public string? RootRelativePath
    {
        get
        {
            var rootRelativePath = Value[Root.Length..];
            return string.IsNullOrWhiteSpace(rootRelativePath) ? null : rootRelativePath;
        }
    }

    public string StringTarget => _target.ToString().ToUpper();

    // Gives safety - we can never go higher than self unless higher up root specified. Helps to stay inside bounds.
    private static void CheckRootPathAndChildPathCompatibility(CleanPath rootPath, CleanPath childPath)
    {
        if (ReferenceEquals(rootPath, childPath)) return;

        if (rootPath.Length > childPath.Length)
        {
            throw new InvalidOperationException(ErrorMessages.RootPathTooLong); // Path out of bounds.
        }

        if (!childPath.Value[..rootPath.Length].Equals(rootPath.Value, StringComparison.InvariantCultureIgnoreCase))
        {
            throw new InvalidOperationException(ErrorMessages.RootPathNotInChildPath);
        }
    }

    public CleanPath CanWrite()
    {
        if (_target is Target.Source)
            throw new InvalidOperationException(ErrorMessages.NullOrSourceTargetReadOnlyError);

        return this;
    }

    public CleanPath CreateChild(string path)
    {
        return new CleanPath(path, Rules, _target)
        {
            Root = Root
        };
    }

    public string[] AsParts()
    {
        var asParts = Value.Split(Rules.Separator);
        return asParts.Where(a => !string.IsNullOrWhiteSpace(a)).ToArray();
    }

    public IEnumerable<string> AllRootRelativeParentPaths()
    {
        var current = RootRelativePath;

        if (current == null)
            yield break;

        var lastPathSeparatorIndex = current.LastIndexOf(Rules.Separator);

        while (lastPathSeparatorIndex > 0)
        {
            yield return current = current[..lastPathSeparatorIndex];
            lastPathSeparatorIndex = current.LastIndexOf(Rules.Separator);
        }
    }

    public CleanPath WithNewRoot(CleanPath newRoot)
    {
        return newRoot.CreateChild(newRoot.Value + RootRelativePath);
    }

    public int CompareTo(CleanPath? other)
    {
        return LongestStringFirstComparer.Static.Compare(Value, other?.Value);
    }

    public override bool Equals(object? obj)
    {
        if (obj is not CleanPath path)
        {
            return false;
        }

        return StringComparer.InvariantCultureIgnoreCase.Compare(Value, path.Value) == 0;
    }

    public override int GetHashCode()
    {
        return StringComparer.InvariantCultureIgnoreCase.GetHashCode(Value);
    }

    public override string ToString() => Value;

    public bool ChildOf(CleanPath path)
    {
        if (path.Length >= Length)
        {
            return false;
        }

        return StringComparer.InvariantCultureIgnoreCase.Equals(
            Value[..(path.Length + 1)],
            path.Value + Rules.Separator);
    }
}