using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text.Json;
using System.Text.RegularExpressions;
using Order66.Utilities;

namespace Order66.Models;

[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
public class Rules
{
    public char Separator { get; set; } = Path.DirectorySeparatorChar;

    public List<string> Verbatim { get; set; } = new();

    public List<Regex> Regex { get; set; } = new();

    public List<Regex> DoNotDelete { get; set; } = new();

    public static Rules Validate(Rules? rules)
    {
        if (rules == null)
        {
            throw new FormatException("Failed to load rules JSON");
        }

        if (rules.Separator != '/' && rules.Separator != '\\')
        {
            throw new FormatException("Bad separator in rules JSON file.");
        }

        return rules;
    }

    public bool IsExcluded(CleanPath fullPath)
    {
        var relativePath = fullPath.RootRelativePath;

        if (relativePath == null)
            throw new InvalidOperationException(
                $"Cannot determine relative path for path: '{fullPath}', with root: '{fullPath.Root}'");

        foreach (var rule in Verbatim)
        {
            if (relativePath.Contains(rule))
            {
                return true;
            }
        }

        foreach (var rule in Regex)
        {
            if (rule.IsMatch(relativePath))
            {
                return true;
            }
        }

        return false;
    }

    public bool CanRemove(CleanPath fullPath)
    {
        var relativePath = fullPath.RootRelativePath;

        if (relativePath == null)
            throw new InvalidOperationException(
                $"Cannot determine relative path for path: '{fullPath}', with root: '{fullPath.Root}'");

        foreach (var rule in DoNotDelete)
        {
            if (rule.IsMatch(relativePath))
            {
                return false;
            }
        }

        return true;
    }

    public static Rules FromPathOrDefault(string? path)
    {
        var rules = new Rules();

        if (!string.IsNullOrWhiteSpace(path))
        {
            rules = Validate(
                JsonSerializer.Deserialize<Rules>(
                    File.ReadAllText(path),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true,
                        Converters = { new RegexConverter() }
                    }
                )
            );
        }

        return rules;
    }
}