﻿namespace Order66.Models;

public record LabeledData<T>(string Label, T Data);