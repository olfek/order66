using System.Collections.Generic;

namespace Order66.Models;

public class RecursionReport
{
    private bool _parentDirExists;

    private bool _filesExist;

    private bool _modified;

    public readonly List<RecursionReport> Children = new();

    public RecursionReport Update(RecursionReport other)
    {
        ParentDirExists = other.ParentDirExists;
        FilesExist = other.FilesExist;
        Modified = other.Modified;
        Children.Add(other);
        return this;
    }

    public bool ParentDirExists
    {
        get => _parentDirExists;
        set
        {
            if (value)
            {
                _parentDirExists = value;
            }
        }
    }

    public bool FilesExist
    {
        get => _filesExist;
        set
        {
            if (value)
            {
                _filesExist = value;
            }
        }
    }

    public bool Modified
    {
        get => _modified;
        set
        {
            if (value)
            {
                _modified = value;
            }
        }
    }
}