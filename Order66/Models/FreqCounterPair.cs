﻿using Order66.Utilities;

namespace Order66.Models;

public class FreqCounterPair
{
    public readonly FreqCounter<IOEvent> MethodCallCounter;

    public readonly FreqCounter<IOEvent> OtherEventCounter;

    public FreqCounterPair(
        FreqCounter<IOEvent>? otherEventCounter,
        FreqCounter<IOEvent> methodCallCounter)
    {
        OtherEventCounter = otherEventCounter ?? new FreqCounter<IOEvent>();
        MethodCallCounter = methodCallCounter;
    }
}