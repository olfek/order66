using System;

namespace Order66.Models;

public class PathDoesNotExistException : Exception
{
    public CleanPath Path { get; }

    public PathDoesNotExistException(CleanPath path) : base(path.Value)
    {
        Path = path;
    }
}