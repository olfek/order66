﻿using System;

namespace Order66.Models;

public sealed class IOEvent : IComparable<IOEvent>
{
    public readonly string Description;

    public IOEvent(string description)
    {
        Description = description;
    }

    public int CompareTo(IOEvent? other)
    {
        return StringComparer.InvariantCultureIgnoreCase.Compare(Description, other?.Description);
    }

    public override int GetHashCode()
    {
        return StringComparer.InvariantCultureIgnoreCase.GetHashCode(Description);
    }

    public override bool Equals(object? obj)
    {
        return CompareTo(obj as IOEvent) == 0;
    }

    public override string ToString() => Description;
}