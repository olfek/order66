﻿namespace Order66.Models;

public record FileMoveInfo(CleanPath Before, CleanPath After);