﻿namespace Order66.Models;

public record SingleUsePath(CleanPath Path)
{
    public bool Used = false;
}