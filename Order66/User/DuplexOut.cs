using System.IO;
using System.Text;

namespace Order66.User;

public abstract class DuplexOut : TextWriter
{
    private static readonly Encoding ChosenEncoding = new UTF8Encoding(false);

    protected readonly TextWriter? File;

    protected DuplexOut()
    {
    }

    protected DuplexOut(TextWriter file)
    {
        File = file;
    }

    protected DuplexOut(Stream stream) :
        this(new StreamWriter(stream, ChosenEncoding) { NewLine = "\n" })
    {
        Stream = stream;
    }

    public static TextWriter? StdOut { get; set; }

    public Stream? Stream { get; }

    public override Encoding Encoding => ChosenEncoding;

    public override void Write(char value)
    {
        StdOut?.Write(value);
        File?.Write(value);
    }

    public override void Flush()
    {
        StdOut?.Flush();
        File?.Flush();
    }

    public override void Close()
    {
        File?.Close();
    }
}