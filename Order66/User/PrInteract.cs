using System;
using System.Globalization;
using Order66.General;
using Order66.Models;

namespace Order66.User;

public class PrInteract
{
    private readonly Out _out;

    private int _pathPrintCount;

    public PrInteract(Out @out)
    {
        _out = @out;
    }

    public void StageSeparator()
    {
        _out.WriteLineFileAndStdOut(
            $"{Environment.NewLine}==========================={Environment.NewLine}");
    }

    public void Path(IOEvent ev, params CleanPath[] paths)
    {
        if (_out.FileAvailable())
            _pathPrintCount++;

        var desc = ev.Description.PadLeft(IOEvents.GetMaxLength(), ' ');

        for (var index = 0; index < paths.Length; index++)
        {
            var cleanPath = paths[index];
            _out.WriteFile($"{desc}  {index + 1}: ");
            _out.WriteLineFile(
                !ReferenceEquals(cleanPath.Root, cleanPath) // Could also be a `RootRelativePath` null check
                    ? cleanPath.StringTarget + cleanPath.RootRelativePath
                    : $"{cleanPath.Value} ({cleanPath.StringTarget})");
        }
    }

    public void Header(string header, int stageCount)
    {
        header = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(header);
        _out.WriteLineFileAndStdOut($"*** STAGE {stageCount}: {header} ***{Environment.NewLine}");
    }

    public void SpaceAfterPrintedPaths()
    {
        if (_pathPrintCount > 0) _out.WriteLineFile(); // Space after paths printed in file.
    }

    public void PrintProgress(Progress progress)
    {
        _out.WriteLineFileAndStdOut($"Paths read: {progress.ProgressCount}");
    }

    public PrInteract Reset() => new(_out);

    public void UserConfirmationPrompt()
    {
        _out.WriteFileAndStdOut("Happy? (y/n) ");
        var input = Console.ReadKey().KeyChar.ToString().ToLower();
        _out.WriteFileAndStdOut($"{Environment.NewLine}{Environment.NewLine}");

        if (input.ToLowerInvariant() == "y") return;

        _out.WriteLineFileAndStdOut("Exiting ...");
        Environment.Exit(0);
    }

    public CleanPath? GetTargetPath()
    {
        _out.WriteLineFileAndStdOut(Environment.NewLine + "Enter another target path or press ENTER to exit");
        var readInput = Console.ReadLine();
        return string.IsNullOrWhiteSpace(readInput) ? null : new CleanPath(readInput);
    }
}