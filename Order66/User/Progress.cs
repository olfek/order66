﻿using System;
using System.IO;

namespace Order66.User;

public sealed class Progress
{
    public int ProgressCount { get; private set; }

    public void ReportProgress()
    {
        ProgressCount++;

        if (DuplexOut.StdOut == null) return; // True for unit tests.

        Console.Write($"Paths read: {ProgressCount}");

        try
        {
            Console.SetCursorPosition(0, Console.CursorTop);
        }
        catch (IOException a) when (a.Message == "The handle is invalid.")
        {
            // Do nothing.
        }
    }

    public void ClearConsole()
    {
        try
        {
            var cursorTop = Console.CursorTop;
            Console.SetCursorPosition(0, cursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, cursorTop);
        }
        catch (IOException a) when (a.Message == "The handle is invalid.")
        {
            // Do nothing.
        }
    }
}