using System.IO;

namespace Order66.User;

public class Out : DuplexOut
{
    public bool FileAvailable() => File != null;

    public PrInteract PrInteract { get; set; }

    public Out()
    {
        PrInteract = new PrInteract(this);
    }

    public Out(TextWriter file) : base(file)
    {
        PrInteract = new PrInteract(this);
    }

    public Out(Stream stream) : base(stream)
    {
        PrInteract = new PrInteract(this);
    }

    public void WriteFile(string text)
    {
        File?.Write(text);
    }

    public void WriteFileAndStdOut(string text)
    {
        Write(text);
    }

    public void WriteLineFile(string text = "")
    {
        if (string.IsNullOrEmpty(text))
        {
            File?.WriteLine();
        }
        else
        {
            File?.WriteLine(text);
        }
    }

    public void WriteLineFileAndStdOut(string text = "")
    {
        if (string.IsNullOrEmpty(text))
        {
            WriteLine();
        }
        else
        {
            WriteLine(text);
        }
    }
}