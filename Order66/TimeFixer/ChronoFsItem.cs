using Order66.Models;

namespace Order66.TimeFixer;

public record ChronoFsItem(CleanPath Path, Times Times, bool IsFile)
{
    public readonly CleanPath Path = Path;

    public readonly Times Times = Times;

    public readonly bool IsFile = IsFile;
}