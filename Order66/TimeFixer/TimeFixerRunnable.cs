using System;
using System.Collections.Generic;
using System.IO;
using Order66.CLIOptions;
using Order66.Enums;
using Order66.Interfaces;
using Order66.Models;
using Order66.User;
using Order66.Utilities;

namespace Order66.TimeFixer;

public class TimeFixerRunnable : IRunnable<TimeFixerOptions>
{
    public bool TestMode { get; init; }

    private readonly IOInterface _io;

    private readonly List<ChronoFsItem> _fsItems = new();

    private Progress _progress;

    private readonly Out _out;

    public TimeFixerRunnable(IOInterface io)
    {
        _out = io.GetOut();
        _io = io;
        _progress = new Progress();
    }

    public void Run(TimeFixerOptions timeFixerOptions)
    {
        if (!TestMode)
        {
            TableGenerator.FromObjectProps(_out, timeFixerOptions);
            _out.PrInteract.UserConfirmationPrompt();

            if (timeFixerOptions.Source != null)
                timeFixerOptions.Source = Path.GetFullPath(timeFixerOptions.Source);
            if (timeFixerOptions.Target != null)
                timeFixerOptions.Target = Path.GetFullPath(timeFixerOptions.Target);
            if (timeFixerOptions.Template != null)
                timeFixerOptions.Template = Path.GetFullPath(timeFixerOptions.Template);
        }

        if (string.IsNullOrWhiteSpace(timeFixerOptions.Source) || string.IsNullOrWhiteSpace(timeFixerOptions.Target))
        {
            _out.WriteLineFileAndStdOut($"{nameof(timeFixerOptions.Source)} and/or {nameof(timeFixerOptions.Target)} is null");
        }
        else
        {
            var rules = Rules.FromPathOrDefault(timeFixerOptions.Rules);

            _out.WriteLineFileAndStdOut("Collecting times from source ...");
            BuildTimesIndexFromSourcePath(
                new CleanPath(timeFixerOptions.Source, rules),
                timeFixerOptions.Template != null ? new CleanPath(timeFixerOptions.Template, rules) : null);

            PostRun();

            NextTargetPath(new CleanPath(timeFixerOptions.Target, rules, Target.Output));
        }
    }

    private void PostRun()
    {
        _progress.ClearConsole();
        _out.PrInteract.SpaceAfterPrintedPaths();
        _out.PrInteract.PrintProgress(_progress);
        _out.PrInteract = _out.PrInteract.Reset();
        _progress = new Progress();
    }

    private void NextTargetPath(CleanPath? targetPath)
    {
        if (targetPath == null) return;

        _out.WriteLineFileAndStdOut($"{Environment.NewLine}Applying times to `{targetPath}`");

        var success = WriteTimesIndexToTargetPath(targetPath);

        if (!success)
        {
            _out.WriteLineFileAndStdOut($"Something went wrong in `{targetPath}`. Aborting.");
        }

        PostRun();

        if (!TestMode)
            // ReSharper disable once TailRecursiveCall
            NextTargetPath(_out.PrInteract.GetTargetPath());
    }

    private void BuildTimesIndexFromSourcePath(CleanPath sourcePath, CleanPath? templatePath)
    {
        foreach (var dynPath in _io.ListDir(templatePath ?? sourcePath))
        {
            _progress.ReportProgress();

            if (sourcePath.Rules.IsExcluded(dynPath)) continue;

            var childPath = templatePath != null ? dynPath.WithNewRoot(sourcePath) : dynPath;

            if (_io.DirExists(childPath))
            {
                if (templatePath != null)
                    BuildTimesIndexFromSourcePath(sourcePath, dynPath);
                else
                    BuildTimesIndexFromSourcePath(childPath, null);
                _fsItems.Add(new ChronoFsItem(childPath, _io.ReadDirTimes(childPath), false));
            }
            else if (_io.FileExists(childPath))
            {
                _fsItems.Add(new ChronoFsItem(childPath, _io.ReadFileTimes(childPath), true));
            }
        }
    }

    private bool WriteTimesIndexToTargetPath(CleanPath targetPath)
    {
        foreach (var item in _fsItems)
        {
            // Don't need to check if excluded here because we are iterating
            // on set of items that have already been checked for exclusion.

            _progress.ReportProgress();

            var subTargetPath = item.Path.WithNewRoot(targetPath);

            if (string.IsNullOrWhiteSpace(subTargetPath.Value))
            {
                _out.WriteLineFileAndStdOut(
                    $"SUB TARGET path is null, empty or whitespace. Original SUB SOURCE path was {item.Path}.");
                return false;
            }

            try
            {
                // TODO: Is something not existing ok? idk.
                if (item.IsFile)
                {
                    _io.WriteFileTimes(subTargetPath, item.Times);
                }
                else
                {
                    _io.WriteDirTimes(subTargetPath, item.Times);
                }
            }
            catch (Exception e)
            {
                _out.WriteLineFileAndStdOut(e.ToString());
                return false;
            }
        }

        return true;
    }
}