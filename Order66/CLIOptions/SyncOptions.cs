using System.Diagnostics.CodeAnalysis;
using CommandLine;

namespace Order66.CLIOptions;

// https://github.com/commandlineparser/commandline/issues/702#issuecomment-881143338

[Verb("Sync", HelpText = "Sync stuff")]
[SuppressMessage("ReSharper", "PropertyCanBeMadeInitOnly.Global")]
[SuppressMessage("ReSharper", "RedundantDefaultMemberInitializer")]
public class SyncOptions
{
    public SyncOptions()
    {
        // Note, overrides default values set at initialization.
        IgnoreEmptyDirsInSource = true;
        RemoveEmptyDirsInOutput = true;
        RemoveStuffFromOutputThatIsExcluded = true;
        RemoveStuffFromOutputThatIsNotInSource = true;
    }

    // +-----------------------------------------+
    // |  Simple behaviour modifications below.  |
    // +-----------------------------------------+

    [Option(
        nameof(Source),
        Default = null,
        Required = true,
        HelpText = "The source")]
    public string? Source { get; set; } = null; // Keep this AND attribute default value, improves readability.

    [Option(
        nameof(Output),
        Default = null,
        Required = true,
        HelpText = "The output")]
    public string? Output { get; set; } = null;

    [Option(
        nameof(Template),
        Default = null,
        Required = false,
        HelpText = $"The directory whose paths will be interpreted as `{nameof(Source)}` paths.")]
    public string? Template { get; set; } = null;

    [Option(
        nameof(Rules),
        Default = null,
        Required = false,
        HelpText = "The rules JSON")]
    public string? Rules { get; set; } = null;

    [Option(
        nameof(DryRun),
        Default = BoolType.True,
        Required = false,
        HelpText = "Run without making any changes")]
    public BoolType DryRun { get; set; } = BoolType.True;

    // +-----------------------------------------+
    // |  Nuanced behavior modifications below.  |
    // +-----------------------------------------+

    [Option(
        nameof(RemoveStuffFromOutputThatIsExcluded),
        Default = false,
        Required = false,
        HelpText = "Self explanatory")]
    public bool RemoveStuffFromOutputThatIsExcluded { get; set; } = false; // This default not used, overriden by constructor.

    [Option(
        nameof(RemoveStuffFromOutputThatIsNotInSource),
        Default = false,
        Required = false,
        HelpText = "Self explanatory")]
    public bool RemoveStuffFromOutputThatIsNotInSource { get; set; } = false; // This default not used, overriden by constructor.

    [Option(
        nameof(RemoveEmptyDirsInOutput),
        Default = false,
        Required = false,
        HelpText = "Self explanatory")]
    public bool RemoveEmptyDirsInOutput { get; set; } = false; // This default not used, overriden by constructor.

    [Option(
        nameof(IgnoreEmptyDirsInSource),
        Default = false,
        Required = false,
        HelpText = "Self explanatory")]
    public bool IgnoreEmptyDirsInSource { get; set; } = false; // This default not used, overriden by constructor.
}