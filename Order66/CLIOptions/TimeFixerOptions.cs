﻿using System.Diagnostics.CodeAnalysis;
using CommandLine;

namespace Order66.CLIOptions;

[Verb("TimeFixer", HelpText = "Fix times in case of bad sync")]
[SuppressMessage("ReSharper", "PropertyCanBeMadeInitOnly.Global")]
[SuppressMessage("ReSharper", "RedundantDefaultMemberInitializer")]
public class TimeFixerOptions
{
    [Option(
        nameof(Source),
        Default = null,
        Required = true,
        HelpText = "The source path")]
    public string? Source { get; set; } = null;

    [Option(
        nameof(Target),
        Default = null,
        Required = true,
        HelpText = "The target path")]
    public string? Target { get; set; } = null;

    [Option(
        nameof(Template),
        Default = null,
        Required = false,
        HelpText = $"The directory whose paths will be interpreted as `{nameof(Source)}` paths.")]
    public string? Template { get; set; } = null;

    [Option(
        nameof(Rules),
        Default = null,
        Required = false,
        HelpText = "The rules JSON")]
    public string? Rules { get; set; } = null;
}