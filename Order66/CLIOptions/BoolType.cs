﻿namespace Order66.CLIOptions;

public enum BoolType
{
    False, // Default because 0
    True
}