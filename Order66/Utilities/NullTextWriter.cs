﻿using System.IO;
using System.Text;

namespace Order66.Utilities;

public class NullTextWriter : TextWriter
{
    public override Encoding Encoding { get; } = Encoding.UTF8;

    public override void Write(char value)
    {
    }
}