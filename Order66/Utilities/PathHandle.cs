using System;
using Order66.Models;

namespace Order66.Utilities;

public record PathHandle(CleanPath root)
{
    private CleanPath? _cleanPath;

    public Lazy<string>? Value { get; set; }

    public CleanPath Get
    {
        get
        {
            if (_cleanPath != null) return _cleanPath;
            return _cleanPath = root.CreateChild(Value!.Value);
        }
    }
}