using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommandLine;
using Order66.Interfaces;

namespace Order66.Utilities;

public static class Helpers
{
    public static async Task<string> GetMD5(Stream stream, CancellationToken ct)
    {
        var bs = await MD5.Create().ComputeHashAsync(stream, ct);

        var sb = new StringBuilder();
        foreach (var b in bs)
        {
            sb.Append(b.ToString("X2"));
        }
        return sb.ToString();
    }

    public static ParserResult<object> Register<T>(this ParserResult<object> parserResult, IRunnable<T> runnable)
    {
        parserResult.WithParsed<T>(runnable.Run);
        return parserResult;
    }
}