using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace Order66.Utilities;

class RegexConverter : JsonConverter<Regex>
{
    public override Regex Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var stringRegex = reader.GetString();

        if (string.IsNullOrWhiteSpace(stringRegex))
        {
            throw new JsonException($"Regular expression empty, null or whitespace.");
        }

        try
        {
            return new Regex(stringRegex);
        }
        catch (RegexParseException)
        {
            throw new JsonException($"-->  {stringRegex}  <-- is not a valid regular expression.");
        }
    }

    public override void Write(Utf8JsonWriter writer, Regex value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString());
    }
}
