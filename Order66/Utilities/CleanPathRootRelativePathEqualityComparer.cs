using System;
using System.Collections.Generic;
using Order66.Models;

namespace Order66.Utilities;

public class CleanPathRootRelativePathEqualityComparer : IEqualityComparer<CleanPath>
{
    private CleanPathRootRelativePathEqualityComparer()
    {
    }

    public bool Equals(CleanPath? x, CleanPath? y)
    {
        if (x is null || y is null)
        {
            // Even if both are NULL, we say they are NOT equal in this context because dependent logic
            // will likely be expecting non-NULL references in the positive case.
            return false;
        }

        return StringComparer.InvariantCultureIgnoreCase.Compare(x.RootRelativePath, y.RootRelativePath) == 0;
    }

    public int GetHashCode(CleanPath obj) => obj.GetHashCode();

    public static readonly CleanPathRootRelativePathEqualityComparer Static = new();
}