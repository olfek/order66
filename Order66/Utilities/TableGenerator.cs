using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleTables;
using Order66.Models;
using Order66.User;

namespace Order66.Utilities;

public static class TableGenerator
{
    public static ConsoleTable FromIOEvents(
        Out @out,
        List<LabeledData<FreqCounterPair>> perStageEventCount)
    {
        var table = new ConsoleTable
        {
            Options =
            {
                OutputTo = @out
            }
        };

        table.AddColumn(new[] { "IO EVENT" });
        table.AddColumn(perStageEventCount.Select(labeledData => labeledData.Label));

        var sortedEventLabels = new SortedSet<IOEvent>();

        foreach (var labeledData in perStageEventCount)
        {
            foreach (var eventLabel in labeledData.Data.OtherEventCounter.GetReadOnlyCount().Keys)
            {
                sortedEventLabels.Add(eventLabel);
            }

            foreach (var eventLabel in labeledData.Data.MethodCallCounter.GetReadOnlyCount().Keys)
            {
                sortedEventLabels.Add(eventLabel);
            }
        }

        foreach (var sortedEventLabel in sortedEventLabels)
        {
            var counts = new List<object>();

            foreach (var labeledData in perStageEventCount)
            {
                labeledData.Data.OtherEventCounter.GetReadOnlyCount()
                    .TryGetValue(sortedEventLabel, out var otherCount);
                labeledData.Data.MethodCallCounter.GetReadOnlyCount()
                    .TryGetValue(sortedEventLabel, out var methodCount);

                counts.Add(Math.Max(otherCount, methodCount));
            }

            table.AddRow(counts.Prepend(sortedEventLabel).ToArray());
        }

        return table;
    }

    public static void FromObjectProps(Out @out, object options)
    {
        var table = new ConsoleTable("OPTION", "VALUE") { Options = { OutputTo = @out } };

        foreach (var prop in options.GetType().GetProperties())
        {
            var value = prop.GetValue(options, null);
            table.AddRow(prop.Name, value==null ? "NOT PROVIDED" : $"\"{value}\"");
        }

        table.Write(Format.Alternative);
    }
}