using System;
using System.Collections.Generic;

namespace Order66.Utilities;

/// <summary>
/// Count the frequency of an object of type <see cref="T"/>
/// </summary>
public class FreqCounter<T> where T : notnull
{
    private bool _closed;

    private readonly Dictionary<T, int> _count;

    public FreqCounter()
    {
        if (typeof(T) == typeof(string))
        {
            _count = new Dictionary<T, int>(StringComparer.InvariantCultureIgnoreCase as IEqualityComparer<T>);
        }
        else
        {
            _count = new Dictionary<T, int>();
        }
    }

    public void Count(T str)
    {
        if (_closed) return;
        _count[str] = _count.GetValueOrDefault(str, 0) + 1;
    }

    public IReadOnlyDictionary<T, int> GetReadOnlyCount()
    {
        Close();
        return _count;
    }

    public void Close() => _closed = true;
}