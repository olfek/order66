using System;
using System.Collections.Generic;
using Order66.Models;

namespace Order66.Utilities;

public class IOCassette
{
    private readonly Dictionary<CleanPath, Dictionary<CleanPath, bool>> _dirState = new();
    private readonly Dictionary<CleanPath, Dictionary<CleanPath, bool>> _fileState = new();

    private static Dictionary<CleanPath, bool> GetParentPathDictionary(
        CleanPath path,
        IDictionary<CleanPath, Dictionary<CleanPath, bool>> state,
        out (CleanPath ParentPath, string Name) tuple)
    {
        tuple = path.GetParentPathAndNameTuple();
        if (state.TryGetValue(tuple.ParentPath, out var items))
            return items;
        return state[tuple.ParentPath] = new Dictionary<CleanPath, bool>();
    }

    public void MarkFileExists(CleanPath path) => GetParentPathDictionary(path, _fileState, out _)[path] = true;

    public void MarkFileDoesNotExist(CleanPath path) => GetParentPathDictionary(path, _fileState, out _)[path] = false;

    public void MarkDirExists(CleanPath path, out (CleanPath ParentPath, string Name) tuple) =>
        GetParentPathDictionary(path, _dirState, out tuple)[path] = true;

    public void MarkDirDoesNotExist(CleanPath path) => GetParentPathDictionary(path, _dirState, out _)[path] = false;

    public bool IsHierarchyIntact(CleanPath path)
    {
        foreach (var (capturedDirPathParent, capturedDirPathDict) in _dirState)
        {
            if (!path.ChildOf(capturedDirPathParent)) continue;

            foreach (var (capturedDirPath, exists) in capturedDirPathDict)
            {
                if (!path.ChildOf(capturedDirPath)) continue;
                if (!exists) return false;
            }
        }

        return true;
    }

    public void ReplayAllStatesDirect(CleanPath directory, Action<CleanPath> exists, Action<CleanPath> doesNotExist)
    {
        foreach (var dictionary in new[] { _fileState, _dirState })
        {
            var parentDirDict = dictionary.GetValueOrDefault(directory); // The "Direct" part.
            if (parentDirDict == null) continue;

            foreach (var (path, pathExists) in parentDirDict)
            {
                if (pathExists)
                {
                    exists(path);
                }
                else
                {
                    doesNotExist(path);
                }
            }
        }
    }

    public void ReplayFileStateIndirect(CleanPath directory, Action<CleanPath> exists, Action<CleanPath> doesNotExist)
    {
        foreach (var (parentDir, parentDirDict) in _fileState)
        {
            if (!parentDir.ChildOf(directory) && !Equals(parentDir, directory)) continue;

            foreach (var (filePath, fileExists) in parentDirDict)
            {
                if (fileExists)
                    exists(filePath);
                else
                    doesNotExist(filePath);
            }
        }
    }

    public bool FileExists(CleanPath path, out bool exists) =>
        GetParentPathDictionary(path, _fileState, out _).TryGetValue(path, out exists);

    public bool DirExists(CleanPath path, out bool exists) =>
        GetParentPathDictionary(path, _dirState, out _).TryGetValue(path, out exists);
}