﻿using System;
using System.Collections.Generic;

namespace Order66.Utilities;

public class LongestStringFirstComparer : IComparer<string>
{
    private readonly StringComparer _stringComparer;

    private LongestStringFirstComparer(StringComparer stringComparer)
    {
        _stringComparer = stringComparer;
    }

    public int Compare(string? x, string? y)
    {
        if (x == null && y != null)
            return 1;

        if (x != null && y == null)
            return -1;

        if (x == null && y == null)
            return 0;

        if (x!.Length == y!.Length)
            return _stringComparer.Compare(x, y);

        return x.Length < y.Length ? 1 : -1;
    }

    public static readonly LongestStringFirstComparer Static =
        new(StringComparer.InvariantCultureIgnoreCase);
}