apt update
apt install -y zip

git fetch --tags

TAG_NAME=$(git tag --points-at "$CI_COMMIT_SHA" | grep "^v" | head -n 1)

echo "TAG_NAME=$TAG_NAME"
echo "CI_PIPELINE_SOURCE=$CI_PIPELINE_SOURCE"
echo "CI_COMMIT_SHA=$CI_COMMIT_SHA"
echo "CI_COMMIT_TAG=$CI_COMMIT_TAG"
echo "CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME"
echo "CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH"
echo "CI_DEFAULT_BRANCH=$CI_DEFAULT_BRANCH"

if [ -z "$TAG_NAME" ] || [ "$CI_COMMIT_BRANCH" != "$CI_DEFAULT_BRANCH" ]; then
    echo "=== 🔴 NOT CREATING RELEASE 🔴 ==="
    exit
fi

echo "=== 🎉 CREATING RELEASE 🎉 ==="

dotnet build Order66 --self-contained true --no-restore -c Release --arch x64 --os linux &&
dotnet build Order66 --self-contained true --no-restore -c Release --arch x64 --os win &&
dotnet build Order66 --self-contained true --no-restore -c Release --arch x86 --os win &&
dotnet build Order66 --self-contained true --no-restore -c Release --arch x64 --os osx

dotnetBuildCommandReturnValue=$?
if [ $dotnetBuildCommandReturnValue -ne 0 ]; then
    exit $dotnetBuildCommandReturnValue
fi

LINUX_X64_NAME="order66-$TAG_NAME-linux-x64.zip"
  WIN_X64_NAME="order66-$TAG_NAME-win-x64.zip"
  WIN_X86_NAME="order66-$TAG_NAME-win-x86.zip"
  OSX_X64_NAME="order66-$TAG_NAME-osx-x64.zip"

zip -r -j $LINUX_X64_NAME ./Order66/bin/Release/net6.0/linux-x64 README.md
zip -r -j $WIN_X64_NAME   ./Order66/bin/Release/net6.0/win-x64   README.md
zip -r -j $WIN_X86_NAME   ./Order66/bin/Release/net6.0/win-x86   README.md
zip -r -j $OSX_X64_NAME   ./Order66/bin/Release/net6.0/osx-x64   README.md

URL="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/generic/Order66/$TAG_NAME"
QPARAMS="?select=package_file"

function UploadFile ()
{
    curl -s --header "JOB-TOKEN: $CI_JOB_TOKEN" \
        --upload-file "$1" "$URL/$1$QPARAMS" > /dev/null && echo "Uploaded: $1"
}

UploadFile $LINUX_X64_NAME
UploadFile $WIN_X64_NAME
UploadFile $WIN_X86_NAME
UploadFile $OSX_X64_NAME
