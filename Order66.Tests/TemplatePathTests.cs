using System.Collections.Immutable;
using Order66.CLIOptions;
using Order66.Models;
using Order66.Sync_;
using Order66.Tests.Support;
using Order66.Tests.Support.VirtualFileSystem;
using Order66.TimeFixer;
using Order66.Utilities;
using Xunit;

namespace Order66.Tests;

public class TemplatePathTests
{
    [Fact]
    public void SyncWriteOutput_WithTemplatePath()
    {
        var source = new DirItem
        {
            Name = "source",
            ["EmptyDir"] = new DirItem(),
            ["one"] = new DirItem
            {
                ["two"] = new FileItem(),
                ["three"] = new FileItem(),
                ["four"] = new FileItem()
            },
            ["a"] = new DirItem
            {
                ["b"] = new DirItem
                {
                    ["c"] = new DirItem
                    {
                        ["z"] = new FileItem(),
                        ["zz"] = new FileItem(),
                        ["zzz"] = new FileItem()
                    }
                }
            },
            ["mario"] = new FileItem(),
            ["sonic"] = new FileItem(),
            ["luigi"] = new FileItem()
        };

        var output = new DirItem
        {
            Name = "output"
        };

        var template = new DirItem
        {
            Name = "template",
            ["EmptyDir"] = new DirItem(),
            ["one"] = new DirItem
            {
                ["three"] = new FileItem()
            },
            ["a"] = new DirItem
            {
                ["b"] = new DirItem
                {
                    ["c"] = new DirItem
                    {
                        ["zzz"] = new FileItem()
                    }
                }
            },
            ["sonic"] = new FileItem(),
        };

        var io = new TestIO(source, output, template);

        var sync = new Sync(io, source.Path, output.Path)
        {
            Options = new SyncOptions
            {
                IgnoreEmptyDirsInSource = false,
                Template = "/template"
            }
        };

        sync.ExecuteWriteOutput();

        var allTemplatePaths = io.FileSystem.GetDir(nameof(template))
            .GetAllContentRecursively().ToImmutableSortedSet();
        var allOutputPaths = io.FileSystem.GetDir(nameof(output))
            .GetAllContentRecursively().ToImmutableSortedSet();

        Assert.Equal(allTemplatePaths.Count, allOutputPaths.Count);

        using var allOutputPathsEt = allOutputPaths.GetEnumerator();
        foreach (var templatePath in allTemplatePaths)
        {
            var sourceThing = io.FileSystem.GetThing(templatePath.WithNewRoot(new CleanPath(source.Path)));
            var templateThing = io.FileSystem.GetThing(templatePath);

            var advanced = allOutputPathsEt.MoveNext();
            Assert.True(advanced);
            Assert.True(CleanPathRootRelativePathEqualityComparer.Static.Equals(templatePath, allOutputPathsEt.Current));

            if (io.FileSystem.GetThing(allOutputPathsEt.Current) is FileItem outputThing)
            {
                Assert.True(sourceThing.Equals(outputThing) && !templateThing.Equals(outputThing));
            }
        }
    }

    [Fact]
    public void TimeFixer_WithTemplatePath()
    {
        var source = new DirItem
        {
            Name = "source",
            ["one"] = new DirItem { Time = 1 },
            ["two"] = new FileItem { Time = 2 },
            ["three"] = new FileItem(),
            ["moo"] = new DirItem
            {
                Time = 3,
                ["you"] = new FileItem(),
                ["foo"] = new DirItem
                {
                    Time = 5,
                    ["rhino"] = new DirItem { Time = 6 },
                    ["lion"] = new FileItem(),
                    ["shoo"] = new DirItem
                    {
                        Time = 8,
                        ["hoo"] = new FileItem { Time = 9 }
                    }
                }
            }
        };

        var output = new DirItem
        {
            Name = "output",
            ["one"] = new DirItem(),
            ["two"] = new FileItem(),
            ["three"] = new FileItem(),
            ["moo"] = new DirItem
            {
                ["you"] = new FileItem(),
                ["foo"] = new DirItem
                {
                    ["rhino"] = new DirItem(),
                    ["lion"] = new FileItem(),
                    ["shoo"] = new DirItem
                    {
                        ["hoo"] = new FileItem()
                    }
                }
            }
        };

        var template = new DirItem
        {
            Name = "template",
            ["one"] = new DirItem(),
            ["two"] = new FileItem(),
            ["moo"] = new DirItem
            {
                ["foo"] = new DirItem
                {
                    ["rhino"] = new DirItem(),
                    ["shoo"] = new DirItem
                    {
                        ["hoo"] = new FileItem()
                    }
                }
            }
        };

        var io = new TestIO(source, output, template);

        new TimeFixerRunnable(io) { TestMode = true }.Run(new TimeFixerOptions
        {
            Source = source.Path,
            Target = output.Path,
            Template = template.Path
        });

        var allTemplatePaths = io.FileSystem.GetDir(nameof(template))
            .GetAllContentRecursively().ToArray();
        var allOutputPaths = io.FileSystem.GetDir(nameof(output))
            .GetAllContentRecursively().ToArray();
        var allSourcePaths = io.FileSystem.GetDir(nameof(source))
            .GetAllContentRecursively().ToArray();

        Array.Sort(allTemplatePaths);
        Array.Sort(allOutputPaths);

        Assert.Equal(allOutputPaths.Length, allSourcePaths.Length);
        Assert.NotEqual(allTemplatePaths.Length, allOutputPaths.Length);

        var allTemplatePathsEt = allTemplatePaths.GetEnumerator();
        Assert.True(allTemplatePathsEt.MoveNext());

        var timesGreaterThanZero = 0;
        var timesEqualToZero = 0;

        foreach (var outputPath in allOutputPaths)
        {
            var sourceThing = io.FileSystem.GetThing(outputPath.WithNewRoot(new CleanPath(source.Path)));
            var outputThing = io.FileSystem.GetThing(outputPath);

            if (
                CleanPathRootRelativePathEqualityComparer.Static.Equals(
                    outputPath,
                    (CleanPath?)allTemplatePathsEt.Current))
            {
                timesGreaterThanZero++;
                Assert.True(outputThing.Time > 0);
                allTemplatePathsEt.MoveNext();
            }
            else
            {
                timesEqualToZero++;
                Assert.True(outputThing.Time == 0);
            }

            Assert.True(outputThing.Time == sourceThing.Time);
        }

        Assert.Equal(allTemplatePaths.Length, timesGreaterThanZero);
        Assert.Equal(allOutputPaths.Length - allTemplatePaths.Length, timesEqualToZero);
    }
}