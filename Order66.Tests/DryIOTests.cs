﻿using Order66.Enums;
using Order66.IO;
using Order66.Models;
using Order66.Sync_;
using Order66.Tests.Support;
using Order66.Tests.Support.VirtualFileSystem;
using Order66.Utilities;
using Xunit;

namespace Order66.Tests;

public class DryIOTests
{
    [Fact]
    public void SimplePathsReplay()
    {
        var io = new DryIO(null);

        var a = new CleanPath("/x").CreateChild("/x/y/z");
        var b = new CleanPath("/a", target: Target.Output).CreateChild("/a/b/c");

        io.Cassette.MarkFileExists(a);
        var abPath = b.GetParentPathAndNameTuple().ParentPath;
        io.CreateDir(abPath);
        io.CopyFile(a, b);

        var paths0 = io.ListDir(new CleanPath("/a"));
        var paths1 = io.ListDir(new CleanPath("/a/b"));

        var paths3 = io.ListAllFileSystemItems(new CleanPath("/a"));
        var paths4 = io.ListAllFileSystemItems(new CleanPath("/a/b"));
        var paths5 = io.ListAllFileSystemItems(new CleanPath("/a/b/c"));

        var paths6 = io.ListAllFileSystemItems(new CleanPath("/lol"));

        Assert.Equal(b, Assert.Single(paths1));
        Assert.Equal(b, Assert.Single(paths3));
        Assert.Equal(b, Assert.Single(paths4));

        Assert.Equal(abPath, Assert.Single(paths0));
        Assert.Empty(paths5);
        Assert.Empty(paths6);
    }

    [Fact]
    public void FullPathsReplay()
    {
        var lol = new FileItem();

        var source = new DirItem
        {
            Name = "x",
            ["b"] = new DirItem
            {
                ["c"] = lol
            }
        };

        var output = new DirItem
        {
            Name = "a"
        };

        var io = new DryIO(null, new TestIO(source, output));

        var sync = new Sync(io, "/x", "/a");
        sync.Execute();

        var abPath = sync.OutputRoot.CreateChild("/a/b");
        var abcPath = sync.OutputRoot.CreateChild("/a/b/c");

        var stuff0 = io.ListDir(new CleanPath("/a"));
        var stuff1 = io.ListDir(abPath);

        var stuff2 = io.ListAllFileSystemItems(new CleanPath("/a"));
        var stuff3 = io.ListAllFileSystemItems(new CleanPath("/a/b"));

        Assert.Equal(abPath, Assert.Single(stuff0));

        Assert.Equal(abcPath, Assert.Single(stuff1));
        Assert.Equal(abcPath, Assert.Single(stuff2));
        Assert.Equal(abcPath, Assert.Single(stuff3));

        Assert.True(io.FileExists(abcPath));
        Assert.True(io.DirExists(abPath));
    }

    [Fact]
    public void Prioritize_DryIO_CapturedStateForReplay()
    {
        var myFile = new FileItem();
        var mySubDir = new DirItem();
        var dir = new DirItem
        {
            Name = "MyDir",
            ["MyFile"] = myFile,
            ["MySubDir"] = mySubDir
        };

        var testIO = new TestIO(dir);
        var dryIO = new DryIO(testIO.GetOut(), testIO);

        var myDirPath = new CleanPath(dir.Path, target: Target.Output);
        var myFilePath = myDirPath.CreateChild(myFile.Path);
        var mySubDirPath = myDirPath.CreateChild(mySubDir.Path);

        dryIO.DeleteFile(myFilePath);
        dryIO.DeleteDir(mySubDirPath);

        Assert.False(dryIO.FileExists(myFilePath));
        Assert.False(dryIO.DirExists(mySubDirPath));
        Assert.False(myDirPath.ChildOf(myDirPath));
        Assert.Empty(dryIO.ListAllFileSystemItems(myDirPath));
        Assert.Empty(dryIO.ListDir(myDirPath));
    }

    [Fact]
    public void ConsiderHierarchy_OnReplayOf_CapturedState()
    {
        var root = new CleanPath("/MyDir", target: Target.Output, rules: new Rules {Separator = '/'});

        var mySubDir = new PathHandle(root);
        var mySubDir2 = new PathHandle(root);
        var myFile = new PathHandle(root);
        var bat = new PathHandle(root);
        var mat = new PathHandle(root);

        var dir = new DirItem
        {
            Name = root.GetName(),
            ["MySubDir", mySubDir] = new DirItem
            {
                ["MyFile", myFile] = new FileItem()
            },
            ["MySubDir2", mySubDir2] = new DirItem
            {
                ["Bat", bat] = new DirItem
                {
                    ["Mat", mat] = new FileItem()
                }
            }
        };

        var testIO = new TestIO(dir);
        var dryIO = new DryIO(testIO.GetOut(), testIO);

        Assert.True(
            new HashSet<CleanPath> { myFile.Get, mat.Get }
                .SetEquals(
                    dryIO.ListAllFileSystemItems(root).ToHashSet()));
        Assert.True(
            new HashSet<CleanPath> { mySubDir.Get, mySubDir2.Get }
                .SetEquals(
                    dryIO.ListDir(root).ToHashSet()));

        dryIO.DeleteDir(mySubDir.Get);
        Assert.Equal(mat.Get, Assert.Single(dryIO.ListAllFileSystemItems(root)));
        Assert.Equal(mySubDir2.Get, Assert.Single(dryIO.ListDir(root)));
        Assert.False(dryIO.FileExists(myFile.Get));
        Assert.False(dryIO.DirExists(mySubDir.Get));

        Assert.Equal(mat.Get, Assert.Single(dryIO.ListDir(bat.Get)));
        dryIO.DeleteDir(mySubDir2.Get);
        Assert.False(dryIO.DirExists(mySubDir2.Get));
        Assert.False(dryIO.DirExists(bat.Get));
        Assert.False(dryIO.FileExists(mat.Get));
        Assert.Throws<PathDoesNotExistException>(() => { dryIO.ListDir(bat.Get); });
        Assert.Throws<PathDoesNotExistException>(() => { dryIO.ListAllFileSystemItems(bat.Get); });

        Assert.Empty(dryIO.ListAllFileSystemItems(root));
        Assert.Empty(dryIO.ListDir(root));
    }
}