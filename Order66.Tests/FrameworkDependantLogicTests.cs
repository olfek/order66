﻿using Order66.Models;
using Xunit;

namespace Order66.Tests;

public class FrameworkDependantLogicTests
{
    [Fact]
    public void IOEvent_CompareTo_CaseInsensitive()
    {
        var expectedSubset = new SortedSet<IOEvent>
        {
            new("A"),
            new("H")
        };

        var set = new SortedSet<IOEvent>
        {
            new("H"),
            new("h"),
            new("A")
        };

        Assert.Equal(2, set.Count);
        Assert.Equal(expectedSubset, set);
    }

    [Fact]
    public void CleanPath_Equals_CaseInsensitive()
    {
        var dictionary = new Dictionary<CleanPath, bool>
        {
            { new CleanPath("/l"), false }
        };

        Assert.Throws<ArgumentException>(() =>
        {
            dictionary.Add(new CleanPath("/L"), false);
        });

        Assert.Single(dictionary);
    }
}