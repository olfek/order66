using Order66.Enums;
using Xunit;
using Order66.General;
using Order66.Models;

namespace Order66.Tests;

public class CleanPathTests
{
    [Fact]
    public void RootRelativePath()
    {
        var rules = new Rules
        {
            Separator = '/'
        };

        // Good case #1
        Assert.Equal(
            "/thing",
            new CleanPath("/a/path//////", rules)
                .CreateChild("/a/path/thing")
                .RootRelativePath
        );

        // Good case #2
        Assert.Equal(
            "/and/this/is/the/child/path",
            new CleanPath("/this/is/the/parent/path", rules) // result should be relative to this (the ROOT)
                .CreateChild("/this/is/the/parent/path/and/this/is") // not relative to this
                .CreateChild("/this/is/the/parent/path/and/this/is/the/child/path")
                .RootRelativePath
        );

        // Child path not child of root path
        Assert.Equal(
            ErrorMessages.RootPathNotInChildPath,
            Assert.Throws<InvalidOperationException>(() =>
            {
                var _ = new CleanPath("/lol", rules)
                    .CreateChild("/this/is/a/totally/different/path")
                    .RootRelativePath;
            }).Message
        );

        // Root path with trailing slash /
        Assert.Equal(
            "/and/this/is/the/child/path",
            new CleanPath("/this/is/the/parent/path/", rules)
                .CreateChild("/this/is/the/parent/path/and/this/is/the/child/path")
                .RootRelativePath
        );

        // Root path with trailing slash \
        Assert.Equal(
            "/and/this/is/the/child/path",
            new CleanPath("/this/is/the/parent/path\\", rules)
                .CreateChild("/this/is/the/parent/path/and/this/is/the/child/path")
                .RootRelativePath
        );

        // Root path longer than child path
        Assert.Equal(
            ErrorMessages.RootPathTooLong,
            Assert.Throws<InvalidOperationException>(() =>
            {
                var _ = new CleanPath("/this/is/the/parent/path/which/is/longer/than/the/child/path", rules)
                    .CreateChild("/this/is/the/parent/path") // child path shorter than parent path
                    .RootRelativePath;
            }).Message
        );

        Assert.Null(new CleanPath("ha", rules).RootRelativePath);
    }

    [Fact]
    public void SeparatorReplacement()
    {
        // Separator replacement test.
        var path1 = new CleanPath(@"\ha\ha\ha", new Rules
        {
            Separator = '/'
        });
        Assert.Equal("/ha/ha/ha", path1.Value);

        // No separator replacement test.
        var path2 = new CleanPath(@"\ha\ha\ha", new Rules
        {
            Separator = '\\'
        });
        Assert.Equal(@"\ha\ha\ha", path2.Value);
    }

    [Fact]
    public void SourceTargetSafety()
    {
        const string message = ErrorMessages.NullOrSourceTargetReadOnlyError;
        // ReSharper disable once RedundantArgumentDefaultValue
        var pathWithTarget = new CleanPath("hello", target: Target.Source);
        var pathWithNoTarget = new CleanPath("hello");

        {
            var ex1 = Assert.Throws<InvalidOperationException>(() =>
            {
                pathWithTarget.CanWrite();
            });

            var ex2 = Assert.Throws<InvalidOperationException>(() =>
            {
                pathWithTarget.CanWrite();
            });

            var ex3 = Assert.Throws<InvalidOperationException>(() =>
            {
                pathWithTarget.CanWrite();
            });

            Assert.Equal(message, ex1.Message);
            Assert.Equal(message, ex2.Message);
            Assert.Equal(message, ex3.Message);
        }

        {
            var ex1 = Assert.Throws<InvalidOperationException>(() =>
            {
                pathWithNoTarget.CanWrite();
            });

            var ex2 = Assert.Throws<InvalidOperationException>(() =>
            {
                pathWithNoTarget.CanWrite();
            });

            var ex3 = Assert.Throws<InvalidOperationException>(() =>
            {
                pathWithNoTarget.CanWrite();
            });

            Assert.Equal(message, ex1.Message);
            Assert.Equal(message, ex2.Message);
            Assert.Equal(message, ex3.Message);
        }

        var _ = pathWithTarget.Value; // Don't need these two lines.
        var __ = pathWithNoTarget.Value;
    }

    [Fact]
    public void ParentPath()
    {
        var rules = new Rules { Separator = '/' };
        var root = new CleanPath("my/root/path", rules);

        {
            var path = root.CreateChild("my/root/path/something/inside/a");

            var one = path.GetParentPathAndNameTuple();
            Assert.Equal("my/root/path/something/inside", one.ParentPath.Value);
            Assert.Equal("a", one.Name);

            var two = path.GetRootRelativeParentPathAndNameTuple();
            Assert.Equal("/something/inside", two.RootRelativeParentPath);
            Assert.Equal("a", two.Name);

            Assert.Equal(new []
            {
                @"/something/inside",
                @"/something"
            }, path.AllRootRelativeParentPaths());
        }

        {
            var path = root.CreateChild("my/root/path/b");

            var one = path.GetParentPathAndNameTuple();
            Assert.Equal("my/root/path", one.ParentPath.Value);
            Assert.Equal("b", one.Name);

            var two = path.GetRootRelativeParentPathAndNameTuple();
            Assert.Null(two.RootRelativeParentPath);
            Assert.Equal("b", two.Name);

            Assert.Equal(Array.Empty<string>(), path.AllRootRelativeParentPaths());
        }

        {
            var path = new CleanPath("my/root/path", rules);

            Assert.Equal(ErrorMessages.RootPathTooLong,
                Assert.Throws<InvalidOperationException>(() => path.GetParentPathAndNameTuple()).Message
            );

            Assert.Equal(ErrorMessages.RootPathTooLong,
                Assert.Throws<InvalidOperationException>(() => path.GetRootRelativeParentPathAndNameTuple()).Message
            );

            Assert.Equal(Array.Empty<string>(), path.AllRootRelativeParentPaths());
        }
    }
}
