using Order66.Models;
using Order66.Tests.Support.VirtualFileSystem;

namespace Order66.Tests.Support;

public class TestIO : SafeTestIO
{
    public TestIO(params DirItem[] roots) : base(roots)
    {
    }

    protected override void _CreateDir(CleanPath path)
    {
        var pathParts = path.AsParts();
        var current = FileSystem;
        DirItem? lastDirCreated = null;

        foreach (var pathPart in pathParts)
        {
            try
            {
                var newDir = new DirItem();
                current[pathPart] = newDir;
                lastDirCreated = newDir;
            }
            catch (DirAlreadyExistsException)
            {
            }

            current = current.GetDir(pathPart);
        }

        lastDirCreated?.Touch();
    }

    protected override void _DeleteFile(CleanPath path)
    {
        var bundle = FileSystem.GetFile(path);
        if (bundle.Name != null && bundle.Parent != null)
        {
            bundle.Parent.Remove(bundle.Name);
        }
    }

    protected override void _DeleteDir(CleanPath path)
    {
        var bundle = FileSystem.GetDir(path);
        if (bundle.Name != null && bundle.Parent != null)
        {
            bundle.Parent.Remove(bundle.Name);
        }
    }

    protected override void _CopyFile(CleanPath a, CleanPath b)
    {
        var fileToCopy = FileSystem.GetFile(a);
        var outputItem = FileSystem.GetDir(b, 1);

        outputItem.Item[outputItem.RemainingPathPart] = fileToCopy.Clone();

        outputItem.Item.Touch();
    }

    protected override void _MoveFile(CleanPath a, CleanPath b)
    {
        var fileToMove = FileSystem.GetFile(a);
        var outputItem = FileSystem.GetDir(b, 1);

        fileToMove.Parent?.Remove(fileToMove.Name!);
        outputItem.Item[outputItem.RemainingPathPart] = fileToMove;

        outputItem.Item.Touch();
    }

    protected override void _WriteDirTimes(CleanPath path, Times times)
    {
        if (times.Access != times.Write && times.Write != times.Create)
        {
            throw new Exception("Expected all times to be the same under test conditions.");
        }
        FileSystem.GetDir(path).Time = (int) new DateTimeOffset(times.Access).ToUnixTimeSeconds();
    }

    protected override void _WriteFileTimes(CleanPath path, Times times)
    {
        if (times.Access != times.Write && times.Write != times.Create)
        {
            throw new Exception("Expected all times to be the same under test conditions.");
        }
        FileSystem.GetFile(path).Time = (int) new DateTimeOffset(times.Access).ToUnixTimeSeconds();
    }
}
