﻿using Order66.Models;
using Order66.Utilities;
using Xunit;

namespace Order66.Tests.Support;

public static class Helpers
{
    public static void ExpectMethodCalls(
        IReadOnlyDictionary<IOEvent, int> count,
        params IOEvent[] expectedMethodsCalls)
    {
        Assert.True(
            count.Keys.Select(a => a.Description).ToHashSet()
                .SetEquals(
                    expectedMethodsCalls.Select(a => a.Description)));
    }

    public static Dictionary<IOEvent, int> Merge(
        this IEnumerable<FreqCounter<IOEvent>> stringOccurrenceCounters)
    {
        var merged = new Dictionary<IOEvent, int>();

        foreach (var stringOccurrenceCounter in stringOccurrenceCounters)
        {
            foreach (var (str,strCount) in stringOccurrenceCounter.GetReadOnlyCount())
            {
                merged[str] = strCount + merged.GetValueOrDefault(str);
            }
        }

        return merged;
    }
}