using System.Collections.ObjectModel;
using System.Security.Cryptography;
using Order66.Interfaces;
using Order66.IO;
using Order66.Models;
using Order66.Tests.Support.VirtualFileSystem;
using Order66.User;

namespace Order66.Tests.Support;

public class SafeTestIO : BaseIO, IExposedIO
{
    public readonly DirItem FileSystem;

    public SafeTestIO(params DirItem[] roots) : base(new Out())
    {
        FileSystem = DirItem.AddRoots(roots.Cast<FsItem>().ToArray());
    }

    public Collection<CleanPath> ExListDir(CleanPath directoryPath)
    {
        try
        {
            return _ListDir(directoryPath);
        }
        catch (DirNotFoundException)
        {
            throw new DirectoryNotFoundException();
        }
    }

    public Collection<CleanPath> ExListAllFileSystemItems(CleanPath directoryPath)
    {
        try
        {
            return _ListAllFileSystemItems(directoryPath);
        }
        catch (DirNotFoundException)
        {
            throw new DirectoryNotFoundException();
        }
    }

    public bool ExFileExists(CleanPath path) => _FileExists(path);

    public bool ExDirExists(CleanPath path) => _DirExists(path);

    public Task<string> ExGetHash(CleanPath a, CancellationToken ct) => _GetHash(a, ct);

    protected override Collection<CleanPath> _ListDir(CleanPath directoryPath)
    {
        var item = FileSystem.GetDir(directoryPath);
        var things = item.ChildrenNames.Select(a =>
            directoryPath.Value + directoryPath.Rules.Separator + a
        ).Select(directoryPath.CreateChild).ToList();
        return new Collection<CleanPath>(things);
    }

    protected override Collection<CleanPath> _ListAllFileSystemItems(CleanPath directoryPath)
    {
        var paths = new List<CleanPath>();
        var collection = new Collection<CleanPath>(paths);

        FileSystem
            .GetDir(directoryPath)
            .GetAllContentRecursivelyByType(null, file =>
            {
                paths.Add(directoryPath.CreateChild(file.Path));
            });

        // Removed because we need to be able create deterministic test cases. Fool!
        // while (paths.Count > 0)
        // {
        //     // Randomise the paths list to simulate possible real OS behaviour.
        //     var pathIndex = RandomNumberGenerator.GetInt32(paths.Count);
        //     var element = paths[pathIndex];
        //     paths.RemoveAt(pathIndex);
        //     collection.Add(element);
        // }

        return collection;
    }

    protected override Times _ReadFileTimes(CleanPath path)
    {
        var time = DateTimeOffset.FromUnixTimeSeconds(
            FileSystem.GetFile(path).Time).UtcDateTime;
        return new Times(time, time, time);
    }

    protected override Times _ReadDirTimes(CleanPath path)
    {
        var time = DateTimeOffset.FromUnixTimeSeconds(
            FileSystem.GetDir(path).Time).UtcDateTime;
        return new Times(time, time, time);
    }

    protected override bool _DirExists(CleanPath path)
    {
        try
        {
            FileSystem.GetDir(path);
            return true;
        }
        catch (VirtualFileSystemException)
        {
            return false;
        }
    }

    protected override bool _FileExists(CleanPath path)
    {
        try
        {
            FileSystem.GetFile(path);
            return true;
        }
        catch (VirtualFileSystemException)
        {
            return false;
        }
    }

    protected override Task<string> _GetHash(CleanPath a, CancellationToken ct)
    {
        return Task.FromResult(FileSystem.GetFile(a).Hash);
    }
}