using System.Text;
using Newtonsoft.Json;

namespace Order66.Tests.Support.VirtualFileSystem;

public abstract class FsItem
{
    public bool Root = false;

    private DirItem? _parent;

    [JsonIgnore]
    public DirItem? Parent
    {
        get => _parent;
        set
        {
            var parentBefore = _parent;
            _parent = value;

            if (parentBefore != null)
            {
                throw new ItemReusedException(this);
            }
        }
    }

    public int Time;

    public string? Name;

    protected FsItem() : this(null, null) { }

    protected FsItem(DirItem? parent, string? name)
    {
        Parent = parent;
        Name = name;
    }

    public string Path
    {
        get
        {
            var sb = new StringBuilder();
            var current = this;

            while (current?.Name != null)
            {
                sb.Insert(0, current.Name);
                // Fixed separator is fine here because we don't have access to a dynamic one,
                // and we only need to create some path representation here which will
                // hopefully be used through a `CleanPath` instance which will take care of
                // separator deviation.
                sb.Insert(0, "/");
                current = current.Parent;
            }

            return sb.ToString();
        }
    }

    public virtual bool EqualsOrException(object? obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            if (obj is FsItem ooo)
            {
                throw new NotEqualBecauseException($"Type of {Path} not same as type of {ooo.Path}");
            }

            throw new NotEqualBecauseException($"Object has type {obj?.GetType()} instead of {GetType()}");
        }

        // Make sure tests aren't re-using objects.
        if (ReferenceEquals(this, obj))
        {
            throw new ItemReusedException(this);
        }

        if ((!Root || !((FsItem)obj).Root) && Time != ((FsItem)obj).Time)
        {
            throw new NotEqualBecauseException("Not root and times not equal.");
        }

        return true;
    }

    public override bool Equals(object? obj)
    {
        try
        {
            return EqualsOrException(obj);
        }
        catch (Exception)
        {
            return false;
        }
    }

    public override int GetHashCode()
    {
        // Not sure what this could be.
        // This type is not intended to be used where this method is required to be implemented.
        // We'll be made aware of attempted usage by throwing the below exception.
        throw new NotImplementedException();
    }

    public abstract FsItem Clone();
}
