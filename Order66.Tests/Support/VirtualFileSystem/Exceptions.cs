namespace Order66.Tests.Support.VirtualFileSystem;

public class NotEqualBecauseException : Exception
{
    public NotEqualBecauseException() : base() { }

    public NotEqualBecauseException(string message) : base(message) { }
}

public class VirtualFileSystemException : Exception
{
    public VirtualFileSystemException() : base() { }

    public VirtualFileSystemException(string message) : base(message) { }
};

public class ItemNotFoundException : VirtualFileSystemException
{
    public ItemNotFoundException(string lookupPath, string lookupSubject) :
        base($"Could not find [{lookupSubject}] at path [{lookupPath}]")
    {
    }
}

public class DirNotFoundException : ItemNotFoundException
{
    public DirNotFoundException(string lookupPath, string lookupSubject) : base(lookupPath, lookupSubject)
    {
    }
}

public class FileNotFoundException : ItemNotFoundException
{
    public FileNotFoundException(string lookupPath, string lookupSubject) : base(lookupPath, lookupSubject)
    {
    }
}

public class FileAlreadyExistsException : VirtualFileSystemException {};

public class DirAlreadyExistsException : VirtualFileSystemException {};

public class ItemReusedException : Exception // We do NOT want this caught implicitly through `VirtualFileSystemException`
{
    public readonly FsItem Obj;

    public ItemReusedException(FsItem obj) : base(obj.Path)
    {
        Obj = obj;
    }
};
