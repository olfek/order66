using System.Collections.ObjectModel;
using Order66.Models;
using Order66.Utilities;

namespace Order66.Tests.Support.VirtualFileSystem;

public sealed class DirItem : FsItem
{
    private IEnumerable<FsItem> Children =>
        _storedDirs.Values
            .Concat(
                _storedFiles.Values.Cast<FsItem>());

    public IEnumerable<string> ChildrenNames => _storedDirs.Keys.Concat(_storedFiles.Keys);

    private readonly Dictionary<string, DirItem> _storedDirs = new(StringComparer.InvariantCultureIgnoreCase);

    private readonly Dictionary<string, FileItem> _storedFiles = new(StringComparer.InvariantCultureIgnoreCase);

    public DirItem GetDir(string name)
    {
        if (!_storedDirs.TryGetValue(name, out var item))
        {
            throw new DirNotFoundException(Path, name);
        }

        return item;
    }

    public FileItem GetFile(string name)
    {
        if (!_storedFiles.TryGetValue(name, out var item))
        {
            throw new FileNotFoundException(Path, name);
        }

        return item;
    }

    public DirItem GetDir(CleanPath path) => GetDir(path, 0).Item;

    public FsItemBundle<DirItem> GetDir(CleanPath path, int endPartsCount)
    {
        var parts = path.AsParts();

        int i;
        var currentItem = this;
        for (i = 0; i < parts.Length - endPartsCount; i++)
        {
            var part = parts[i];
            currentItem = currentItem.GetDir(part);
        }

        var remainingPathParts = new ArraySegment<string>(parts, i, parts.Length - i).ToArray();
        return new FsItemBundle<DirItem>(currentItem, remainingPathParts);
    }

    public FileItem GetFile(CleanPath path)
    {
        var dir = GetDir(path, 1);
        return dir.Item.GetFile(dir.RemainingPathPart);
    }

    public FsItem GetThing(CleanPath path)
    {
        var dir = GetDir(path, 1);

        try
        {
            return dir.Item.GetFile(dir.RemainingPathPart);
        }
        catch (FileNotFoundException)
        {
            try
            {
                return dir.Item.GetDir(dir.RemainingPathPart);
            }
            catch (DirNotFoundException)
            {
                throw new ItemNotFoundException(dir.Item.Path, dir.RemainingPathPart);
            }
        }
    }

    private IEnumerable<KeyValuePair<string, FsItem>> GetAllContent()
    {
        foreach (var (key, value) in _storedDirs)
        {
            yield return KeyValuePair.Create(key, (FsItem)value);
        }

        foreach (var (key, value) in _storedFiles)
        {
            yield return KeyValuePair.Create(key, (FsItem)value);
        }
    }

    private bool AllContentTryGetValue(string key, out FsItem? value)
    {
        if (_storedDirs.TryGetValue(key, out var foundDir))
        {
            value = foundDir;
            return true;
        }

        if (_storedFiles.TryGetValue(key, out var foundFile))
        {
            value = foundFile;
            return true;
        }

        value = null;
        return false;
    }

    public override bool EqualsOrException(object? obj)
    {
        if (!base.EqualsOrException(obj))
        {
            throw new NotEqualBecauseException(); // Won't get here. Kept for future reference.
        }

        var other = (DirItem)obj!;
        var source = string.IsNullOrWhiteSpace(Path) ? "[no name]" : Path;
        var target = string.IsNullOrWhiteSpace(other.Path) ? "[no name]" : other.Path;

        if (other._storedDirs.Count != _storedDirs.Count)
        {
            throw new NotEqualBecauseException($"Dir count of {source} not equal to {target}");
        }

        if (other._storedFiles.Count != _storedFiles.Count)
        {
            throw new NotEqualBecauseException($"Files count of {source} not equal to {target}");
        }

        foreach (var (key, value) in GetAllContent())
        {
            if (!other.AllContentTryGetValue(key, out var otherValue))
            {
                throw new NotEqualBecauseException($"{value.Path} does not exist in {target}.");
            }

            if (!value.EqualsOrException(otherValue))
            {
                throw new NotEqualBecauseException(); // Won't get here. Kept for future reference.
            }
        }

        return true;
    }

    public static DirItem AddRoots(params FsItem[] roots)
    {
        var dir = new DirItem();
        foreach (var root in roots)
        {
            if (root.Name == null)
            {
                throw new VirtualFileSystemException();
            }
            dir[root.Name] = root;
            root.Root = true;
        }
        return dir;
    }

    public FsItem this[string itemName]
    {
        set
        {
            if (_storedDirs.ContainsKey(itemName))
            {
                throw new DirAlreadyExistsException();
            }

            if (_storedFiles.ContainsKey(itemName))
            {
                throw new FileAlreadyExistsException();
            }

            value.Parent = this;
            value.Name = itemName;

            switch (value)
            {
                case DirItem valueAsDirItem:
                    _storedDirs[itemName] = valueAsDirItem;
                    break;
                case FileItem valueAsFileItem:
                    _storedFiles[itemName] = valueAsFileItem;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
    }

    public FsItem this[string itemName, PathHandle pathHandle]
    {
        set
        {
            this[itemName] = value;
            pathHandle.Value = new Lazy<string>(() => value.Path);
        }
    }

    public void Remove(string pathPart)
    {
        var toNull = new List<FsItem>();

        if (_storedDirs.Remove(pathPart, out var removedDirItem)) toNull.Add(removedDirItem);
        if (_storedFiles.Remove(pathPart, out var removedFileItem)) toNull.Add(removedFileItem);

        foreach (var fsItem in toNull)
            try
            {
                fsItem.Parent = null;
            }
            catch (ItemReusedException)
            {
                // It's ok here.
                // So it can be used again.
            }

        if (toNull.Any()) Touch();
    }

    public void Touch()
    {
        var current = this;
        while (current != null)
        {
            current.Time = -1;
            current = current.Parent;
        }
    }

    public void GetAllContentRecursivelyByType(
        Action<DirItem>? dirItemCallback = null,
        Action<FileItem>? fileItemCallback = null)
    {
        if (dirItemCallback == null && fileItemCallback == null) return;

        foreach (var item in Children)
        {
            switch (item)
            {
                case DirItem castedDirItem:
                    dirItemCallback?.Invoke(castedDirItem);
                    castedDirItem.GetAllContentRecursivelyByType(dirItemCallback, fileItemCallback);
                    break;
                case FileItem castedFileItem:
                    fileItemCallback?.Invoke(castedFileItem);
                    break;
            }
        }
    }

    public override DirItem Clone()
    {
        throw new NotImplementedException();
    }

    public IEnumerable<CleanPath> GetAllContentRecursively(Rules? rules = null)
    {
        var paths = new List<CleanPath>();
        var rootDirCleanPath = new CleanPath(Path, rules);
        GetAllContentRecursivelyByType(
            item => paths.Add(rootDirCleanPath.CreateChild(item.Path)),
            item => paths.Add(rootDirCleanPath.CreateChild(item.Path)));
        return new Collection<CleanPath>(paths);
    }
}
