using Newtonsoft.Json;

namespace Order66.Tests.Support.VirtualFileSystem;

public sealed class FileItem : FsItem
{
    private static int _hash;

    public string Hash = Interlocked.Increment(ref _hash).ToString();

    public override bool EqualsOrException(object? obj)
    {
        if (!base.EqualsOrException(obj))
        {
            throw new NotEqualBecauseException(); // Won't get here. Kept for future reference.
        }

        var other = (FileItem)obj!;

        if (other.Hash != Hash)
        {
            throw new NotEqualBecauseException("Hash not equal.");
        }

        return true;
    }

    public override FileItem Clone()
    {
        var a = JsonConvert.DeserializeObject<FileItem>(JsonConvert.SerializeObject(this));
        // a.Parent = null; // already NULL
        return a;
    }
}
