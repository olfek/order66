namespace Order66.Tests.Support.VirtualFileSystem;

public class FsItemBundle<T> where T : FsItem
{
    public readonly T Item;

    public readonly IEnumerable<string> RemainingPathParts;

    public string RemainingPathPart => this.RemainingPathParts.Single();

    public FsItemBundle(T item, IEnumerable<string> remainingPathParts)
    {
        this.Item = item;
        this.RemainingPathParts = remainingPathParts;
    }
}
