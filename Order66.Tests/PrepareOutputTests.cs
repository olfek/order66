using Order66.CLIOptions;
using Order66.Models;
using Order66.Sync_;
using Order66.Tests.Support;
using Xunit;
using Order66.Tests.Support.VirtualFileSystem;
using FileNotFoundException = Order66.Tests.Support.VirtualFileSystem.FileNotFoundException;

namespace Order66.Tests;

public class PrepareOutputTests
{
    [Fact]
    public void EmptyDirsRemovedFromOutput()
    {
        var source = new DirItem
        {
            Name = "source",
            ["lots"] = new DirItem
            {
                ["of"] = new DirItem
                {
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        var output = new DirItem
        {
            Name = "output",
            ["lots"] = new DirItem
            {
                ["of"] = new DirItem
                {
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        var result = new DirItem
        {
            Name = "result",
            Time = -1
        };

        new Sync(new TestIO(source, output), source.Path, output.Path).ExecutePrepareOutput();

        Assert.True(output.EqualsOrException(result));
    }

    [Fact]
    public void EmptyDirsRemovedFromOutputTest2()
    {
        var source = new DirItem
        {
            Name = "source",
            ["lots"] = new DirItem
            {
                Time = 903123,
                ["of"] = new DirItem
                {
                    Time = 837623754,
                    ["mi"] = new FileItem { Hash = "101" },
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        var output = new DirItem
        {
            Name = "output",
            ["lots"] = new DirItem
            {
                ["of"] = new DirItem
                {
                    ["mi"] = new FileItem { Hash = "101" },
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        var result = new DirItem
        {
            Name = "result",
            Time = -1,
            ["lots"] = new DirItem
            {
                Time = 903123,
                ["of"] = new DirItem
                {
                    Time = 837623754,
                    ["mi"] = new FileItem { Hash = "101" }
                }
            }
        };

        new Sync(new TestIO(source, output), source.Path, output.Path).ExecutePrepareOutput();

        Assert.True(output.EqualsOrException(result));
    }

    [Fact]
    public void ExcludedFileRemoved_Then_EmptyDirsRemovedFromOutput()
    {
        var source = new DirItem
        {
            Name = "source",
            ["lots"] = new DirItem
            {
                ["of"] = new DirItem
                {
                    ["mmmi"] = new FileItem { Hash = "101" },
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        var output = new DirItem
        {
            Name = "output",
            ["lots"] = new DirItem
            {
                ["of"] = new DirItem
                {
                    ["mmmi"] = new FileItem { Hash = "101" },
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        var result = new DirItem
        {
            Name = "result",
            Time = -1
        };

        new Sync(new TestIO(source, output), source.Path, output.Path, rules: new Rules { Verbatim = { "mmmi" } })
            .ExecutePrepareOutput();

        Assert.True(output.EqualsOrException(result));
    }

    [Fact]
    public void DoNotDeleteEmptyDirsFromOutput()
    {
        var source = new DirItem { Name = "source" };

        var output = new DirItem
        {
            Name = "output",
            ["lots"] = new DirItem
            {
                ["of"] = new DirItem
                {
                    ["mmmi"] = new FileItem { Hash = "101" },
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        var result = new DirItem
        {
            Time = -1,
            Name = "result",
            ["lots"] = new DirItem
            {
                Time = -1,
                ["of"] = new DirItem
                {
                    Time = -1,
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        new Sync(new TestIO(source, output), source.Path, output.Path, rules: new Rules
        {
            Verbatim =
            {
                "mmmi"
            }
        })
        {
            Options = new SyncOptions
            {
                RemoveEmptyDirsInOutput = false,
                RemoveStuffFromOutputThatIsNotInSource = false,
            }
        }.ExecutePrepareOutput();

        Assert.True(output.EqualsOrException(result));
    }

    [Fact]
    public void OutputAndSource_SamePaths_Different_FsItemTypes()
    {
        var source = new DirItem
        {
            Name = "source",
            ["a"] = new DirItem
            {
                Time = 76541,
                ["b"] = new DirItem
                {
                    Time = 184762,
                    ["b1"] = new FileItem(),
                    ["b2"] = new DirItem()
                }
            }
        };

        var output = new DirItem
        {
            Name = "output",
            ["a"] = new DirItem
            {
                ["b"] = new DirItem
                {
                    ["b1"] = new DirItem(),
                    ["b2"] = new FileItem()
                }
            }
        };

        var result = new DirItem
        {
            Name = "result",
            Time = -1,
            ["a"] = new DirItem
            {
                Time = 76541,
                ["b"] = new DirItem
                {
                    Time = 184762
                }
            }
        };

        new Sync(new TestIO(source, output), source.Path, output.Path)
        {
            Options = new SyncOptions
            {
                RemoveEmptyDirsInOutput = false
            }
        }.ExecutePrepareOutput();

        Assert.True(output.EqualsOrException(result));
    }

    [Fact]
    public void TimeRestore()
    {
        var source = new DirItem
        {
            Name = "source",
            Time = 58,
            ["MyDir"] = new DirItem
            {
                Time = 78
            }
        };

        var output = new DirItem
        {
            Name = "output",
            Time = 1001,
            ["MyDir"] = new DirItem
            {
                Time = 5123,
                ["MySubDir"] = new DirItem()
            }
        };

        new Sync(new TestIO(source, output), source.Path, output.Path)
        {
            Options = new SyncOptions
            {
                RemoveStuffFromOutputThatIsNotInSource = true,
                RemoveEmptyDirsInOutput = false
            }
        }.ExecutePrepareOutput();

        Assert.True(output.EqualsOrException(source));
    }

    [Fact]
    public void TimeRestore_NotPossible()
    {
        var source = new DirItem
        {
            Name = "source",
            Time = 58
        };

        var output = new DirItem
        {
            Name = "output",
            Time = 1001,
            ["MyDir"] = new DirItem
            {
                Time = 5123,
                ["MySubDir"] = new DirItem()
            }
        };

        var expectedOutput = new DirItem
        {
            Name = "output",
            Time = -1,
            ["MyDir"] = new DirItem()
            {
                Time = -1
            }
        };

        new Sync(
            new TestIO(source, output), source.Path, output.Path,
            rules: new Rules
                { Verbatim = new List<string> { "MySubDir" } })
        {
            Options = new SyncOptions
            {
                RemoveStuffFromOutputThatIsNotInSource = false,
                RemoveEmptyDirsInOutput = false
            }
        }.ExecutePrepareOutput();

        Assert.True(output.EqualsOrException(expectedOutput));
    }

    [Fact]
    public void EnsureDirectorySiblings_NotAffectedBy_OtherSiblingsChildren()
    {
        var source = new DirItem
        {
            Name = "source",
            ["MyDir1"] = new DirItem
            {
                Time = 1287,
                ["MyFile1"] = new FileItem()
            },
            ["MyDir2"] = new DirItem
            {
                Time = 135,
                ["MyFile1"] = new FileItem()
            },
            ["MyDir3"] = new DirItem()
        };

        var dest = new DirItem
        {
            Name = "output",
            ["MyDir1"] = new DirItem // Dir time restored because children modified.
            {
                ["MyFile1"] = new FileItem(), // File remains, so files exist = true.
                ["MyFile2"] = new FileItem() // File removed, so modified = true.
            },
            ["MyDir2"] = new DirItem // Dir time not restored because children not modified. No longer affected by siblings children "modified = true".
            {
                ["MyFile1"] = new FileItem() // File remains, so files exist = true.
            },
            ["MyDir3"] = new DirItem() // Empty dir removed. No longer affected by siblings children "files exist = true".
        };

        new Sync(new TestIO(source, dest), source.Path, dest.Path)
        {
            Options = new SyncOptions
            {
                RemoveStuffFromOutputThatIsNotInSource = true,
                RemoveEmptyDirsInOutput = true,
            }
        }.ExecutePrepareOutput();

        Assert.Equal(-1, dest.Time);
        Assert.Throws<FileNotFoundException>(() => dest.GetDir("MyDir1").GetFile("MyFile2"));
        Assert.Equal(1287, dest.GetDir("MyDir1").Time);
        Assert.Equal(0, dest.GetDir("MyDir2").Time);
        Assert.Throws<DirNotFoundException>(() => dest.GetDir("MyDir3"));
    }
}