using Order66.CLIOptions;
using Xunit;
using Order66.Tests.Support.VirtualFileSystem;
using Order66.Models;
using Order66.Sync_;
using Order66.Tests.Support;

namespace Order66.Tests;

public class FileTrackingTests
{
    [Fact]
    public void SimpleFileMove()
    {
        var fileToMove = new FileItem();
        var preventDirDeletion = new FileItem();

        var source = new DirItem
        {
            Name = "aa",
            Time = 32,
            ["bb"] = new DirItem
            {
                Time = 14
            },
            ["ll"] = new DirItem
            {
                Time = 501,
                ["00"] = new DirItem
                {
                    Time = 121,
                    ["cc"] = fileToMove
                }
            }
        };

        var output = new DirItem()
        {
            Name = "zz",
            Time = 4000,
            ["uu"] = new DirItem
            {
                Time = 14,
                ["xx"] = new DirItem
                {
                    Time = 867,
                    ["gg"] = new DirItem
                    {
                        Time = 6590,
                        ["PreventDirDeletion"] = preventDirDeletion, // Empty dirs are removed by file tracking logic.
                        ["ThisIsANewFileHeHe"] = fileToMove.Clone()
                    }
                }
            }
        };

        var expected = new DirItem
        {
            Name = "expected",
            Time = -1,
            ["uu"] = new DirItem
            {
                Time = -1,
                ["xx"] = new DirItem
                {
                    Time = -1,
                    ["gg"] = new DirItem
                    {
                        Time = -1,
                        ["PreventDirDeletion"] = preventDirDeletion.Clone()
                    }
                }
            },
            ["ll"] = new DirItem
            {
                Time = 501,
                ["00"] = new DirItem
                {
                    Time = 121,
                    ["cc"] = fileToMove.Clone()
                }
            }
        };

        var app = new Sync(new TestIO(source, output), "\\aa", "\\zz")
        {
            Options = new SyncOptions
            {
                RemoveEmptyDirsInOutput = false,
                IgnoreEmptyDirsInSource = false,
                RemoveStuffFromOutputThatIsNotInSource = false
            }
        };

        app.TrackFileMovesAndMoveFiles();

        Assert.True(output.EqualsOrException(expected));
    }

    [Fact]
    public void AllRootRelativeParentPaths()
    {
        var rules = new Rules
        {
            Separator = '/'
        };

        var allRootRelativeParentPaths = new CleanPath("ROOT", rules)
            .CreateChild("ROOT/one/two/three/four")
            .AllRootRelativeParentPaths()
            .ToArray();

        Assert.Contains("/one", allRootRelativeParentPaths);
        Assert.Contains("/one/two/three", allRootRelativeParentPaths);
        Assert.Contains("/one/two", allRootRelativeParentPaths);
    }

    [Fact]
    public void MultipleAllocation()
    {
        var rules = new Rules
        {
            Separator = '\\'
        };

        var fileToMove = new FileItem();

        var source = new DirItem
        {
            Name = "aa",
            Time = 32,
            ["locationOne"] = new DirItem
            {
                Time = 501,
                ["00"] = new DirItem
                {
                    Time = 121,
                    ["mm"] = fileToMove
                }
            },
            ["locationTwo"] = new DirItem
            {
                Time = 501,
                ["00"] = new DirItem
                {
                    Time = 121,
                    ["nn"] = fileToMove.Clone()
                }
            },
            ["locationThree"] = new DirItem
            {
                Time = 501,
                ["00"] = new DirItem
                {
                    Time = 121,
                    ["bb"] = fileToMove.Clone()
                }
            }
        };

        var dupeFile1 = fileToMove.Clone();
        var dupeFile2 = fileToMove.Clone();
        var dupeFile3 = fileToMove.Clone();
        var dupeFile4 = fileToMove.Clone();

        var output = new DirItem()
        {
            Name = "zz",
            Time = 4000,
            ["DuplicateFile1"] = dupeFile1,
            ["DuplicateFile2"] = dupeFile2,
            ["DuplicateFile3"] = dupeFile3,
            ["DuplicateFile4"] = dupeFile4
        };

        var app = new Sync(new TestIO(source, output), "\\aa", "\\zz", rules: rules)
        {
            Options = new SyncOptions
            {
                RemoveEmptyDirsInOutput = false,
                IgnoreEmptyDirsInSource = false,
                RemoveStuffFromOutputThatIsNotInSource = false
            }
        };

        app.TrackFileMovesAndMoveFiles();

        var dupeFiles = new[] { dupeFile1, dupeFile2, dupeFile3, dupeFile4 };

        Assert.Contains(dupeFiles, a => a.Path == "/zz/locationOne/00/mm");
        Assert.Contains(dupeFiles, a => a.Path == "/zz/locationTwo/00/nn");
        Assert.Contains(dupeFiles, a => a.Path == "/zz/locationThree/00/bb");
        Assert.Contains(dupeFiles, a => a.Path[..^1] == "/zz/DuplicateFile");
    }

    [Fact]
    public void FullAppExecution_WithFileTracking()
    {
        var lol = new FileItem();

        var source = new DirItem
        {
            Name = "source",
            ["joke"] = new DirItem
            {
                ["lol.txt"] = lol
            }
        };

        var output = new DirItem
        {
            Name = "output",
            ["lol.txt"] = lol.Clone()
        };

        new Sync(new TestIO(source, output), source.Path, output.Path).Execute();
    }

    [Fact]
    public void FileTracking_IgnoreCase_ForParentPath()
    {
        var fileToMove = new FileItem();

        var source = new DirItem
        {
            Name = "aa",
            ["ee"] = new DirItem
            {
                ["ff"] = new DirItem
                {
                    ["gg"] = fileToMove.Clone()
                }
            },
            ["bb"] = new DirItem
            {
                ["cc"] = new DirItem
                {
                    ["dd"] = fileToMove.Clone()
                }
            }
        };

        var output = new DirItem()
        {
            Name = "zz",
            ["EE"] = new DirItem
            {
                ["FF"] = new DirItem
                {
                    ["gg"] = fileToMove.Clone()
                }
            },
            ["BB"] = new DirItem
            {
                ["CC"] = new DirItem
                {
                    ["DD"] = fileToMove.Clone()
                }
            }
        };

        var rules = new Rules { Separator = '/' };
        var testIo = new TestIO(source, output);
        var app = new Sync(testIo, "\\aa", "\\zz", rules: rules);

        var report = app.TrackFileMovesAndMoveFiles();

        var moveJob = Assert.Single(report.MoveJobs);
        Assert.Equal("/zz/BB/CC/DD", moveJob.Before.Value);
        Assert.Equal("/zz/bb/cc/dd", moveJob.After.Value);

        Assert.Equal(new SortedSet<string>
        {
            "/BB/CC",
            "/BB",
        }, report.PreMovePaths);

        Assert.Equal(new SortedSet<string>
        {
            "/bb/cc",
            "/bb",
        }, report.PostMovePaths);

        Assert.Equal(2, output.ChildrenNames.Count());
        Assert.Equal(6, output.GetAllContentRecursively().Count());

        var gg = testIo.FileSystem.GetFile(new CleanPath("/zz/ee/ff/gg", rules: rules));
        Assert.Equal("gg", gg.Name);

        var dd = testIo.FileSystem.GetFile(new CleanPath("/zz/bb/cc/dd", rules: rules));
        Assert.Equal("dd", dd.Name);
    }
}