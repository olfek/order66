using Order66.CLIOptions;
using Order66.Models;
using Order66.Sync_;
using Order66.Tests.Support;
using Xunit;
using Order66.Tests.Support.VirtualFileSystem;

namespace Order66.Tests;

public class WriteOutputTests
{
    [Fact]
    public void EmptyDirsInSourceIgnored()
    {
        var source = new DirItem
        {
            Name = "source",
            ["lots"] = new DirItem
            {
                ["of"] = new DirItem
                {
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            },
            ["mifile"] = new FileItem { Hash = "uuu" }
        };

        var output = new DirItem
        {
            Name = "output"
        };

        var result = new DirItem
        {
            Name = "result",
            Time = -1,
            ["mifile"] = new FileItem { Hash = "uuu" }
        };

        new Sync(new TestIO(source, output), source.Path, output.Path).ExecuteWriteOutput();

        Assert.True(output.EqualsOrException(result));
    }

    [Fact]
    public void EmptyDirsInSource_BecauseOf_ExcludedFile_Ignored()
    {
        var source = new DirItem
        {
            Name = "source",
            ["lots"] = new DirItem
            {
                ["of"] = new DirItem
                {
                    ["mifile"] = new FileItem { Hash = "uuu" },
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        var output = new DirItem
        {
            Name = "output"
        };

        var result = new DirItem
        {
            Name = "result"
        };

        new Sync(new TestIO(source, output), source.Path, output.Path, rules: new Rules { Verbatim = { "mifile" } })
            .ExecuteWriteOutput();

        Assert.True(output.EqualsOrException(result));
    }

    [Fact]
    public void DoNotIgnoreEmptyDirsInSource()
    {
        var source = new DirItem
        {
            Name = "source",
            ["lots"] = new DirItem
            {
                ["of"] = new DirItem
                {
                    ["mifile"] = new FileItem { Hash = "uuu" },
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        var output = new DirItem
        {
            Name = "output"
        };

        var result = new DirItem
        {
            Name = "result",
            Time = -1,
            ["lots"] = new DirItem
            {
                ["of"] = new DirItem
                {
                    ["empty"] = new DirItem
                    {
                        ["dirs"] = new DirItem()
                    }
                }
            }
        };

        new Sync(new TestIO(source, output), source.Path, output.Path, rules: new Rules
        {
            Verbatim =
            {
                "mifile"
            }
        })
        {
            Options = new SyncOptions
            {
                IgnoreEmptyDirsInSource = false
            }
        }.ExecuteWriteOutput();

        Assert.True(output.EqualsOrException(result));
    }

    [Fact]
    public void NoUnexpectedIOEvents()
    {
        var source = new DirItem
        {
            Name = "source",
            ["subdir"] = new DirItem
            {
                ["disAFile"] = new FileItem()
            }
        };

        var output = new DirItem{ Name = "output" };

        var sync = new Sync(new TestIO(source, output), source.Path, output.Path);
        sync.Execute();
        Assert.Empty(sync.CleanOutputEventCounter.GetReadOnlyCount());
        Assert.Empty(sync.WriteOutputEventCounter.GetReadOnlyCount());
    }

    [Fact]
    public void NoUnexpectedIOEventsAdvanced()
    {
        var source = new DirItem
        {
            Name = "source",
            ["subDir"] = new DirItem
            {
                ["disAFile"] = new FileItem()
            }
        };

        var output = new DirItem{ Name = "output" };

        var sync = new Sync(new TestIO(source, output), source.Path, output.Path);
        var executePrepareOutput = sync.ExecutePrepareOutput();
        var executeWriteOutput = sync.ExecuteWriteOutput();
        Assert.Empty(sync.CleanOutputEventCounter.GetReadOnlyCount());
        Assert.Empty(sync.WriteOutputEventCounter.GetReadOnlyCount());

        Assert.Empty(executePrepareOutput.Children);
        Assert.All(
            new[]
            {
                executePrepareOutput.Modified, executePrepareOutput.FilesExist, executePrepareOutput.ParentDirExists
            }, Assert.False);

        var subDirRecursionReport = Assert.Single(executeWriteOutput.Children);
        Assert.Empty(subDirRecursionReport.Children);
        Assert.All(
            new[]
            {
                subDirRecursionReport.Modified, subDirRecursionReport.FilesExist,
                subDirRecursionReport.ParentDirExists
            },
            Assert.True);
    }
}