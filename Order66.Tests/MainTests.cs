using Order66.CLIOptions;
using Order66.Enums;
using Order66.General;
using Xunit;
using Order66.Tests.Support.VirtualFileSystem;
using Order66.Models;
using Order66.Sync_;
using Order66.Tests.Support;
using Helpers = Order66.Tests.Support.Helpers;

namespace Order66.Tests;

public class MainTests
{
    [Fact]
    public void CreateDirUpdatesParentsTimes()
    {
        var output = new DirItem
        {
            Name = "aa",
            Time = 32,
            ["bb"] = new DirItem
            {
                Time = 14
            }
        };

        var io = new TestIO(output);
        io.CreateDir(new CleanPath("/aa/bb/cc", target: Target.Output));
        output.GetAllContentRecursivelyByType(dir => Assert.True(dir.Time == -1));
    }

    [Fact]
    public void SimpleFunctionalTest1()
    {
        var source = new DirItem
        {
            Name = "aa",
            Time = 32,
            ["bb"] = new DirItem
            {
                Time = 14,
                ["cc"] = new FileItem()
            }
        };

        var output = new DirItem()
        {
            Name = "zz"
        };

        new Sync(new TestIO(source, output), "\\aa", "\\zz").Execute();

        Assert.True(output.EqualsOrException(source));
    }

    [Fact]
    public void DisallowItemReuse()
    {
        var sharedFileItem = new FileItem();

        var source = new DirItem
        {
            Name = "aa",
            Time = 32,
            ["bb"] = new DirItem
            {
                Time = 14,
                ["cc"] = sharedFileItem
            }
        };

        var exception = Assert.Throws<ItemReusedException>(() =>
        {
            var output = new DirItem
            {
                Name = "zz",
                Time = 32,
                ["bb"] = new DirItem
                {
                    Time = 14,
                    ["cc"] = sharedFileItem
                }
            };
        });

        var dirItem = new DirItem();

        var exception2 = Assert.Throws<ItemReusedException>(() =>
        {
            dirItem.EqualsOrException(dirItem);
        });

        Assert.Equal(sharedFileItem, exception.Obj);
        Assert.Equal(dirItem, exception2.Obj);
    }

    [Fact]
    public void FsItemClone()
    {
        var dirItem = new DirItem();
        var fileItem = new FileItem();

        Assert.Throws<NotImplementedException>(() => dirItem.Clone());

        Assert.Null(fileItem.Clone().Parent);
    }

    [Fact]
    public void FileWithDifferentHashIsReplaced()
    {
        var source = new DirItem
        {
            Name = "aa",
            ["bb"] = new DirItem
            {
                ["lol"] = new FileItem(),
                ["cc"] = new DirItem
                {
                    ["end"] = new FileItem()
                }
            }
        };

        var output = new DirItem
        {
            Name = "zz",
            ["bb"] = new DirItem
            {
                ["lol"] = new FileItem() // Different hash
            }
        };

        new Sync(new TestIO(source, output), "\\aa", "\\zz").Execute();

        Assert.True(output.EqualsOrException(source));
    }

    [Fact]
    public void NoActionsTakenIfDirsEqual()
    {
        var source = new DirItem
        {
            Name = "source",
            Time = 32,
            ["bb"] = new DirItem
            {
                Time = 14,
                ["cc"] = new FileItem { Hash = "1" },
                ["dd"] = new FileItem { Hash = "11" },
                ["ee"] = new FileItem { Hash = "111" },
            }
        };

        var output = new DirItem()
        {
            Name = "output",
            Time = 32,
            ["bb"] = new DirItem
            {
                Time = 14,
                ["cc"] = new FileItem { Hash = "1" },
                ["dd"] = new FileItem { Hash = "11" },
                ["ee"] = new FileItem { Hash = "111" },
            }
        };

        new Sync(new SafeTestIO(source, output), source.Path, output.Path).Execute();

        Assert.True(output.EqualsOrException(source));
    }

    [Fact]
    public void DirTimesRestored_Without_DirCreation()
    {
        var source = new DirItem
        {
            Name = "source",
            Time = 32,
            ["bb"] = new DirItem
            {
                Time = 14,
                ["cc"] = new FileItem { Hash = "1" },
                ["dd"] = new FileItem { Hash = "11" },
                ["ee"] = new FileItem { Hash = "111" },
            }
        };

        var output = new DirItem()
        {
            Name = "output",
            Time = 32,
            ["bb"] = new DirItem
            {
                Time = 14,
                ["cc"] = new FileItem { Hash = "1" },
                ["dd"] = new FileItem { Hash = "11" },
            }
        };

        var io = new TestIO(source, output);

        var stageCounts = new Sync(io, source.Path, output.Path).Execute();

        Helpers.ExpectMethodCalls(
            stageCounts.Select(labeledData => labeledData.Data.MethodCallCounter).Merge(),
            //IOEvents.ListAll,
            IOEvents.List,
            IOEvents.DirExists,
            IOEvents.FileExists,
            //IOEvents.Hash,
            IOEvents.Copy,
            IOEvents.RestoreDirTimes
        );

        Assert.True(output.EqualsOrException(source));
    }

    [Fact]
    public void EmptyDirsNotCreated_And_DirTimesNotRestored()
    {
        var source = new DirItem
        {
            Name = "source",
            Time = 32,
            ["aa"] = new DirItem
            {
                Time = 14,
                ["bb"] = new DirItem
                {
                    Time = 500,
                    ["cc"] = new DirItem
                    {
                        Time = 50,
                    }
                }
            }
        };

        var output = new DirItem()
        {
            Name = "output",
            Time = 32,
            ["aa"] = new DirItem
            {
                Time = 14,
                ["bb"] = new DirItem
                {
                    Time = 500,
                    ["cc"] = new DirItem
                    {
                        Time = 50
                    }
                }
            }
        };

        new Sync(new SafeTestIO(source, output), source.Path, output.Path)
        {
            Options = new SyncOptions
            {
                IgnoreEmptyDirsInSource = false,
                RemoveEmptyDirsInOutput = false
            }
        }.Execute();

        Assert.True(output.EqualsOrException(source));
    }

    [Fact]
    public void DirNotCreated_If_FirstFile_NotIn_Output()
    {
        var source = new DirItem
        {
            Name = "source",
            Time = 32,
            ["aa"] = new DirItem
            {
                Time = 14,
                ["uu"] = new FileItem(),
                ["cc"] = new FileItem { Hash = "303" },
            }
        };

        var output = new DirItem()
        {
            Name = "output",
            Time = 32,
            ["aa"] = new DirItem
            {
                Time = 14,
                ["cc"] = new FileItem { Hash = "303" }
            }
        };

        var io = new TestIO(source, output);

        var stageCounts = new Sync(io, source.Path, output.Path)
        {
            Options = new SyncOptions
            {
                IgnoreEmptyDirsInSource = false,
                RemoveEmptyDirsInOutput = false
            }
        }.Execute();

        Helpers.ExpectMethodCalls(
            stageCounts.Select(labeledData => labeledData.Data.MethodCallCounter).Merge(),
            //IOEvents.ListAll,
            IOEvents.List,
            IOEvents.DirExists,
            IOEvents.FileExists,
            //IOEvents.Hash,
            IOEvents.Copy,
            IOEvents.RestoreDirTimes
        );

        Assert.True(output.EqualsOrException(source));
    }
}
