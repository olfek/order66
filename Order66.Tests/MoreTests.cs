using Order66.CLIOptions;
using Order66.Sync_;
using Order66.Tests.Support;
using Xunit;
using Order66.Tests.Support.VirtualFileSystem;

namespace Order66.Tests;

public class MoreTests
{
    [Fact]
    public void SimpleTest1()
    {
        var source = new DirItem
        {
            Name = "source",
            Time = 32,
            ["aa"] = new DirItem
            {
                Time = 14,
                ["bb"] = new DirItem
                {
                    Time = 500,
                    ["cc"] = new DirItem
                    {
                        Time = 50,
                    }
                }
            }
        };

        var output = new DirItem() { Name = "output" };

        new Sync(new TestIO(source, output), source.Path, output.Path)
        {
            Options = new SyncOptions
            {
                IgnoreEmptyDirsInSource = false,
                RemoveEmptyDirsInOutput = false
            }
        }.Execute();

        Assert.True(output.EqualsOrException(source));
    }

    [Fact]
    public void MultipleEmptyDirs_At_MultipleLevels()
    {
        var source = new DirItem
        {
            Name = "source",
            Time = 32,
            ["aa"] = new DirItem
            {
                Time = 14,
                ["bb"] = new DirItem
                {
                    Time = 500,
                    ["cc"] = new DirItem
                    {
                        Time = 50,
                    },
                    ["ee"] = new DirItem
                    {
                        Time = 435
                    }
                },
                ["dd"] = new DirItem
                {
                    Time = 1001
                }
            }
        };

        var output = new DirItem() { Name = "output" };

        new Sync(new TestIO(source, output), source.Path, output.Path)
        {
            Options = new SyncOptions
            {
                IgnoreEmptyDirsInSource = false,
                RemoveEmptyDirsInOutput = false
            }
        }.Execute();

        Assert.True(output.EqualsOrException(source));
    }

    [Fact]
    public void NotEqualBecause_TypesDifferent()
    {
        var src = new DirItem()
        {
            ["hey"] = new FileItem(),
            ["o"] = new DirItem()
        };

        var trgt = new DirItem()
        {
            ["hey"] = new DirItem(),
            ["o"] = new FileItem()
        };

        var exception = Assert.Throws<NotEqualBecauseException>(() =>
        {
            src.EqualsOrException(trgt);
        });

        Assert.NotNull(exception);
        Assert.Contains("type", exception.Message, StringComparison.InvariantCultureIgnoreCase);
    }

    [Fact]
    public void NotEqualBecause_ItemDoesNotExist()
    {
        var src = new DirItem()
        {
            Name = "SrcDirItem",
            ["hey"] = new FileItem()
        };

        var trgt = new DirItem()
        {
            Name = "TargetDirItem",
            ["o"] = new FileItem()
        };

        var exception = Assert.Throws<NotEqualBecauseException>(() =>
        {
            src.EqualsOrException(trgt);
        });

        Assert.NotNull(exception);
        Assert.Contains("does not exist", exception.Message, StringComparison.InvariantCultureIgnoreCase);
    }

    [Fact]
    public void NotEqualBecause_DirCountDifferent()
    {
        var src = new DirItem()
        {
            Name = "SrcDirItem",
            ["hey"] = new FileItem(),
            ["y"] = new DirItem()
        };

        var trgt = new DirItem()
        {
            Name = "TargetDirItem",
            ["o"] = new FileItem(),
            ["y"] = new FileItem()
        };

        var exception = Assert.Throws<NotEqualBecauseException>(() =>
        {
            src.EqualsOrException(trgt);
        });

        Assert.NotNull(exception);
        Assert.Contains("dir count", exception.Message, StringComparison.InvariantCultureIgnoreCase);
    }

    [Fact]
    public void NotEqualBecause_FileCountDifferent()
    {
        var src = new DirItem()
        {
            Name = "SrcDirItem",
            ["hey"] = new FileItem(),
            ["y"] = new DirItem()
        };

        var trgt = new DirItem()
        {
            Name = "TargetDirItem",
            ["o"] = new FileItem(),
            ["y"] = new FileItem(),
            ["k"] = new DirItem()
        };

        var exception = Assert.Throws<NotEqualBecauseException>(() =>
        {
            src.EqualsOrException(trgt);
        });

        Assert.NotNull(exception);
        Assert.Contains("files count", exception.Message, StringComparison.InvariantCultureIgnoreCase);
    }
}